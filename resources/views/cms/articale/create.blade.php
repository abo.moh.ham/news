
@extends('cms.parent')

@section('title','Create Article')
@section('page-name','Create Article')
@section('main-page','articales')
@section('sub-page','Create Article')

@section('styles')

<!-- Toastr -->
<link rel="stylesheet" href="{{ asset('cms/plugins/summernote/summernote-bs4.css') }}">


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="create_form" action="{{ route('articles.store') }}" method="post" enctype="multipart/form-data" >
                @csrf
                <div class="card-body">

                <div class="form-grup">





                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter titel"  required>
                    {{-- minlength="3" --}}
                </div>

                  <div class="form-group">
                    <label for="shrt_description">Short Description </label>
                    <textarea type="text" class="form-control" id="shrt_description" name="shrt_description" placeholder="Enter short description" required>

                    </textarea>
                  </div>
                  <div class="form-group">
                    <label for="mobile">Full Description</label>
                    <textarea class="textarea" placeholder="Place some text here"
                    rows="10"
                    style="width: 100%; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                     id="full_description" name="full_description" placeholder="Enter full description" required
                    ></textarea>
                  </div>




                  <div class="form-group">
                    <label for="customFile">article File</label>
                    <div class="custom-file">
                        <input type="file" name="image" onchange="loadFile(event)" class="custom-file-input" id="profession-image">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    <br>
                    <img src="" width="200px" id='preview_file' alt="">
                </div>

                <div class="form-group">
                    <label for="first_name">Category</label>
                    <select class="form-control select" name="category_id" id="category_id">
                        @foreach ( $categories as $category )
                        <option value="{{ $category->id }}">{{ $category->name }}</option>

                        @endforeach
                    </select>
                </div>
@if (auth('author')->check())
<input type="hidden" name="author_id" value="{{ auth('author')->id() }}">
@else
<div class="form-group">
    <label for="first_name">Authors</label>
    <select class="form-control select" name="author_id" id="author_id" >
        @foreach ( $authors as $author )
        <option value="{{ $author->id }}">{{ $author->name }}</option>

        @endforeach
    </select>
</div>

@endif


@if (auth('author')->check())
<input type="hidden" name="author_id" value="{{ auth('author')->id() }}">
@else

<div class="form-group">
    <div class="custom-control ">
      <input class="custom-control-input" type="checkbox" id="special" value="1"  name="special">
      <label for="special" class="custom-control-label">Special article</label>
    </div>

  </div>
@endif



</div>
</div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
 <!-- bs-custom-file-input -->
<script src="{{ asset('cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<script src="{{ asset('cms/plugins/summernote/summernote-bs4.min.js') }}"></script>


    <script>
        //  $('#create_form').validate();
        $('#create_form').submit(function(e){
            e.preventDefault();

            var data = new FormData(this);

            axios.post('{{ route('articles.store') }}',data)
            .then(function (response) {
                console.log(response);
                document.getElementById('create_form').reset();
                showToaster(response.data.message, true);
            })
            .catch(function (error) {
                console.log(error.response);
                showToaster(error.response.data.message, false);
            });
        });


          // Summernote
    $('.textarea').summernote({
        height:250
    })

    var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('preview_file');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };

    </script>

@endsection
