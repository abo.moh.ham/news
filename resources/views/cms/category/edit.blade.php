
@extends('cms.parent')

@section('title','Edit category')
@section('page-name','Edit Category')
@section('main-page','Categories')
@section('sub-page','Edit Category ')

@section('styles')

<!-- Toastr -->


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Category</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="edit_form" action="{{ route('categories.create' , $category) }}" method="post" >
                @csrf
                @method('PUT')
                <div class="card-body">

                <div class="form-grup">





                  <div class="form-group">
                    <label for="first_name">Name</label>
                    <input type="text" class="form-control" id="name" value="{{ $category->name }}" name="name" placeholder="Enter name"  required>
                    {{-- minlength="3" --}}
                </div>

                  <div class="form-group">
                    <label for="email">Description</label>
                    <input type="text" class="form-control" id="description" name="description" value="{{ $category->description }}" placeholder="Enter Email">
                  </div>


                  <div class="form-group">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" name="status" class="custom-control-input" id="status"
                        @if($category->status) checked @endif
                        >
                        <label class="custom-control-label" for="status"> Status</label>

                    </div>
                </div>


                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
 <!-- bs-custom-file-input -->
<script src="{{ asset('cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<!-- Toastr -->


    <script>
        //  $('#create_form').validate();
        $('#edit_form').submit(function(e){
            e.preventDefault();

            var data = new FormData(this);

            axios.post('{{ route('categories.update',$category) }}',data)
            .then(function (response) {
                console.log(response);
                showToaster(response.data.message, true);
            })
            .catch(function (error) {
                console.log(error.response);
                showToaster(error.response.data.message, false);
            });
        });


    </script>

@endsection
