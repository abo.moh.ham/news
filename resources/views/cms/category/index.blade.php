@extends('cms.parent')

@section('title','categories')
@section('page-name','Index categories')
@section('main-page','categories')
@section('sub-page','Index')

@section('styles')
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">categories</h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 50px;">
                                <input type="text" name="table_search" class="form-control float-right"
                                    placeholder="Search">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover table-bordered text-nowrap">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>status</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Settings</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                <tr>
                                    <td>{{ $category->id }}</td>
                                    <td>{{ $category->name}}</td>
                                    <td>{{ $category->description}}</td>
                                    <td>{{ $category->status }}</td>

                                    <td>{{ $category->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $category->updated_at->format('Y-m-d') }}</td>
                                    <td>
                                    <div class="btn-group">
                                            <a href="{{ route('categories.edit',$category->id) }}" type="button"
                                                class="btn btn-info"><i class="fas fa-edit"></i></a>
                                            {{-- {{-- @if(Auth::category()->id != $category->id) --}}
                                            {{-- <form role="form" class="destroy" method="POST" action="{{ route('categories.destroy',$category->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                            </form> --}}

                                            {{-- @endif  --}}
                                        {{-- </div> --}}

                                        <a href="#"  onclick="confirmDestroy({{ $category->id }},this)" class="btn btn-danger"><i class="fas fa-trash"></i></a>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        {{-- {{ $categories->links() }} --}}
                        {{-- {{ $categories->render }} --}}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.row -->
    </div>
</section>
@endsection

@section('scripts')
<script>
    function confirmDestroy(id, td){
        console.log('Category ID:'+id);

        Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    destroy(id, td);
  }


});
    }
function destroy(id, td){
             axios.delete('/cms/user/categories/'+id)
            .then(function (response) {
                console.log(response.data);

                swalAlert(response.data);
                td.closest('tr').remove();
            })
            .catch(function (error) {

            console.log(error.response);

            swalAlert(error.response.data);
                // console.log(error);
            })
            .then(function () {
                // always executed
  });
}

function swalAlert(data){

            Swal.fire({
            title: data.title,
            text: data.message,
            icon:data.icon,
            timer: 2000,
            timerProgressBar: false,
                    didOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
            const content = Swal.getHtmlContainer()
            if (content) {
                const b = content.querySelector('b')
                if (b) {
                b.textContent = Swal.getTimerLeft()
                }
            }
            }, 100)
        },
        willClose: () => {
            // clearInterval(timerInterval)
        }
        }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            console.log('I was closed by the timer')
        }
        })
}

</script>
@endsection
