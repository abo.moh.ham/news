@extends('cms.parent')

@section('title','Authors')
@section('page-name','Index Authors')
@section('main-page','Authors')
@section('sub-page','Index')

@section('styles')
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Authors</h3>

                        <div class="card-tools" style="width:510px">
                            <form role="form"action="{{ route('authors.index') }}" method="get" >
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="input-group input-group-sm" style="width: 100%;">
                                            <input type="text" name="search" value="{{ request()->search }}" class="form-control float-right"
                                                placeholder="Search">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="input-group">

                                            <select class="form-control select" name="searchCtegory" value="{{ request()->searchCategory }}" class="form-control float-center" multiple
                                                placeholder="Search Category">
                                                @foreach ( $categories as $category )
                                                <option value="{{ $category->id }}"
                                        @if (request()->searchCtegory==$category->id)
                                        selected

                                         @endif >
                                         {{ $category->name }}



                                                </option>


                                                @endforeach
                                            </select>


                                    </div>

        </div>
        <div class="co-md-2">
            <button type="submit" class="btn btn-success">search</button>
        </div>
                                    </div>


                                </div>

                        </form>

                        </div>


                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover table-bordered text-nowrap">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>categires</th>
                                    <th>Gender</th>
                                    <th>status</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Settings</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($authors->count())

                                @foreach ($authors as $author)
                                {{-- {{ dd($author) }} --}}
                                <tr>
                                    <td>{{ $author->id }}</td>
                                    <td>{{ $author->name}}</td>
                                    <td>{{ $author->email}}</td>
                                    <td>{{ $author->mobile }}</td>
                                    <td>{{ $author->categories->pluck('name')->implode('-') }}</td>
                                    <td><span class="badge bg-success">{{ $author->gender }}</span></td>

                                    <td>{{ $author->status }}</td>

                                    {{-- <td><a href="{{ route('authors.permissions.index',$author->id) }}"
                                            class="btn btn-info">{{ $author->permissions_count }} / Permissions <i
                                                class="fas fa-author-tie"></i></a></td> --}}
                                    {{-- <td>{{ $author->city->name }}</td> --}}
                                    <td>{{ $author->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $author->updated_at->format('Y-m-d') }}</td>
                                    <td>
                                    <div class="btn-group">
                                            <a href="{{ route('authors.edit',$author->id) }}" type="button"
                                                class="btn btn-info"><i class="fas fa-edit"></i></a>

                                        <a href="#"  onclick="confirmDestroy({{ $author->id }},this)" class="btn btn-danger"><i class="fas fa-trash"></i></a>

                                    </td>
                                </tr>
                                @endforeach
                                @else()
                                <tr>
                                    <td colspan="10" class=text-center>
                                        No result
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        {{-- {{ $authors->links() }} --}}
                        {{-- {{ $authors->render }} --}}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.row -->
    </div>
</section>
@endsection

@section('scripts')
<script>
    function confirmDestroy(id, td){
        console.log('Authors ID:'+id);

        Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    destroy(id, td);
  }


});
    }
function destroy(id, td){
             axios.delete('/cms/user/authors/'+id)
            .then(function (response) {
                console.log(response.data);

                swalAlert(response.data);
                td.closest('tr').remove();
            })
            .catch(function (error) {

            console.log(error.response);

            swalAlert(error.response.data);
                // console.log(error);
            })
            .then(function () {
                // always executed
  });
}

function swalAlert(data){

            Swal.fire({
            title: data.title,
            text: data.message,
            icon:data.icon,
            timer: 2000,
            timerProgressBar: false,
                    didOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
            const content = Swal.getHtmlContainer()
            if (content) {
                const b = content.querySelector('b')
                if (b) {
                b.textContent = Swal.getTimerLeft()
                }
            }
            }, 100)
        },
        willClose: () => {
            // clearInterval(timerInterval)
        }
        }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            console.log('I was closed by the timer')
        }
        })
}

</script>
@endsection
