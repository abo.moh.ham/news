
@extends('cms.parent')

@section('title','Edit Author')
@section('page-name','Edit Author')
@section('main-page','Authors')
@section('sub-page','Edit Author ')

@section('styles')

<!-- Toastr -->


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Author</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="edit_form" action="{{ route('authors.update' , $author) }}" method="post" >
                @csrf
                @method('PUT')
                <div class="card-body">

                <div class="form-grup">





                  <div class="form-group">
                    <label for="first_name">Name</label>
                    <input type="text" class="form-control" id="name" value="{{ $author->name }}" name="name" placeholder="Enter name"  required>
                    {{-- minlength="3" --}}
                </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $author->email }}" placeholder="Enter Email">
                  </div>
                  <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input type="tel" class="form-control" id="mobile" name="mobile" value="{{$author->mobile}}" placeholder="Enter Mobile">
                  </div>


                  <div class="form-group">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" name="status" class="custom-control-input" id="status"
                        @if($author->status) checked @endif
                        >
                        <label class="custom-control-label" for="status">Active Status</label>

                    </div>
                </div>

                  <div class="form-group">
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="male" value="M"  name="gender"
                      @if($author->gender == 'M') checked @endif
                      >
                      <label for="male" class="custom-control-label">Male</label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="female" value="F" name="gender"
                      @if($author->gender == 'F') checked @endif
                      >
                      <label for="female" class="custom-control-label">Female</label>
                    </div>
                  </div>
                  {{-- <div class="form-group">
                    <label for="first_name">Category</label>
                    <select class="form-control select" name="category_id[]" id="category_id" multiple>
                        @foreach ( $categories as $category )
                        <option value="{{ $category->id }}"@if($author->category_id == $category->id) selected
                            @endif>
                            {{ $category->name }}</option>

                        @endforeach
                    </select>
                </div> --}}
                <div class="form-group">
                    <label for="first_name">Category</label>
                    <select class="form-control select" name="category_id[]" id="category_id" multiple>
                        @foreach ( $categories  as $category )
                        <option value="{{ $category->id }}"
                            @if ($author->categories->where('id',$category->id)->first() )selected

                            @endif>
                            {{ $category->name }}
                        </option>

                        @endforeach
                    </select>
                </div>



                {{-- <div class="form-group">
                    <label for="first_name">Password</label>
                    <input type="password" class="form-control" id="password"  name="password" placeholder="Enter password"  >

                </div> --}}

                  </div>
            </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
 <!-- bs-custom-file-input -->
<script src="{{ asset('cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<!-- Toastr -->


    <script>
        //  $('#create_form').validate();
        $('#edit_form').submit(function(e){
            e.preventDefault();

            var data = new FormData(this);

            axios.post('{{ route('authors.update',$author) }}',data)
            .then(function (response) {
                console.log(response);
                showToaster(response.data.message, true);
            })
            .catch(function (error) {
                console.log(error.response);
                showToaster(error.response.data.message, false);
            });
        });


    </script>

@endsection
