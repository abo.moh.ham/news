@extends('cms.parent')

@section('title','Admins')
@section('page-name','Index Admins')
@section('main-page','Admins')
@section('sub-page','Index')

@section('styles')
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Admins</h3>

                        <div class="card-tools">
                            <form role="form"  method="get" action="{{ route('admins.index') }}">

                            <div class="input-group input-group-sm" style="width: 200px;">

                                <input type="text" name="search" value="{{ request()->search }}" id="search"class="form-control float-right"
                                    placeholder="Search">

                                <div class="input-group-append">


                                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </div>

                            </div>
                        </form>

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover table-bordered text-nowrap">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>status</th>
                                    <th>Gender</th>
                                    <th>Permissions</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Settings</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($admins->count())
                                    @foreach ($admins as $admin)
                                <tr>
                                    <td>{{ $admin->id }}</td>
                                    <td>{{ $admin->name}}</td>
                                    <td>{{ $admin->email}}</td>
                                    <td>{{ $admin->mobile }}</td>
                                    <td>{{ $admin->status }}</td>
                                    <td><span class="badge bg-success">{{ $admin->gender }}</span></td>
                                    <td><a href="{{ route('admins.permissions.index',$admin->id) }}"
                                            class="btn btn-info">{{ $admin->permissions_count }} / Permissions <i
                                                class="fas fa-user-tie"></i></a></td>
                                    {{-- <td>{{ $admin->city->name }}</td> --}}
                                    <td>{{ $admin->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $admin->updated_at->format('Y-m-d') }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('admins.edit',$admin->id) }}" type="button"
                                                class="btn btn-info"><i class="fas fa-edit"></i></a>
                                            {{-- {{-- @if(Auth::user()->id != $admin->id) --}}
                                            <form role="form" class="destroy" method="POST" action="{{ route('admins.destroy',$admin->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                            </form>

                                            {{-- @endif  --}}
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @else( )
                                <tr>
                                    <td colspan='10'class=text-center >no result</td>
                                </tr>

@endif
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        {{ $admins->links() }}
                        {{-- {{ $admins->render }} --}}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.row -->
    </div>
</section>
@endsection

@section('scripts')
{{-- <script>
    function performDestroy(id, td){
        // confirmDestroy('{{ route('admins.destroy',$admin->id) }}', td)
        axios.delete('{{ route('admins.destroy',$admin->id) }}')
            .then(function (response) {
                console.log(response.data);
                showAlert(response.data);
            })
            .catch(function (error) {
                console.log(error.response);
                showAlert(error.response.data);
            })
            .then(function () {
                // always executed
            });
    }

    function restore(id){

        }
</script> --}}

<script>
// $('#search').submit(function(e){
// e.preventDefault();

// var data = new FormData(this);

// function performStore(){
//     store('route('search')', data)

// }
// });
    $('.destroy').submit(function(e){
        e.preventDefault();
        Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    axios.delete($(this).attr('action'))
            .then(function (response) {
                showToaster(response.data.message,'success');
                window.location.reload();
            })
            .catch(function (error) {
                console.log(error)
                showToaster("error hapen",'error');
            })

  }
})
    })
</script>
@endsection
