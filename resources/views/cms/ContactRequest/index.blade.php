@extends('cms.parent')

@section('title','Contact Request')
@section('page-name','Index ContactRequest')
@section('main-page',' ContactRequest')
@section('sub-page','Index')

@section('styles')
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
   <div class="col-12">
    <div class="card">
      <div class="card-header">
    <h3 class="card-title">Contact Request</h3>
 <div class="card-tools">
  <div class="input-group input-group-sm" style="width: 50px;">
     <input type="text" name="table_search" class="form-control float-right"
 placeholder="Search">

    <div class="input-group-append">
     <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
      </div>
    </div>
     </div>
     </div>
       <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover table-bordered text-nowrap">
      <thead>
                <tr>
                  <th>#</th>
          <th>Title</th>
          <th>Meesage</th>
          <th>Created At</th>
          <th>Updated At</th>
          <th>Settings</th>
      </tr>
  </thead>
  <tbody>
    @foreach ($contacts as $contact)
    <tr>
        <td>{{ $contact->id }}</td>
        <td>{{ $contact->title}}</td>
        <td>{{ $contact->message }}</td>
        <td>{{ $contact->created_at->format('Y-m-d') }}</td>
        <td>{{ $contact->updated_at->format('Y-m-d') }}</td>
              <td>
                <div class="btn-group">  <a href="{{ route('contacts.edit',$contact->id) }}" type="button"
                    class="btn btn-info"><i class="fas fa-edit"></i></a>
                {{-- {{-- @if(Auth::user()->id != $admin->id) --}}
                {{-- <form role="form" class="destroy" method="POST" action="{{ route('contactRequest.destroy',$contact->id) }}">
                   @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>                                             </form> --}}

                    {{-- @endif  --}}
                    <a href="#"  onclick="confirmDestroy({{ $contact->id }},this)" class="btn btn-danger"><i class="fas fa-trash"></i></a>

           </div>
       </td>
   </tr>
   @endforeach
</tbody>
</table>
</div>
<div class="card-footer clearfix">
{{-- {{ $admins->links() }} --}}
{{-- {{ $admins->render }} --}}
</div>
<!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
<!-- /.row -->
</div>
</section>
@endsection

@section('scripts')
<script>
    function confirmDestroy(id, td){
        console.log('City ID:'+id);

        Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    destroy(id, td);
  }


});
    }
function destroy(id, td){
             axios.delete('/cms/user/contacts/'+id)
            .then(function (response) {
                console.log(response.data);

                swalAlert(response.data);
                td.closest('tr').remove();
            })
            .catch(function (error) {

            console.log(error.response);

            swalAlert(error.response.data);
                // console.log(error);
            })
            .then(function () {
                // always executed
  });
}

function swalAlert(data){

            Swal.fire({
            title: data.title,
            text: data.message,
            icon:data.icon,
            timer: 2000,
            timerProgressBar: false,
                    didOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
            const content = Swal.getHtmlContainer()
            if (content) {
                const b = content.querySelector('b')
                if (b) {
                b.textContent = Swal.getTimerLeft()
                }
            }
            }, 100)
        },
        willClose: () => {
            // clearInterval(timerInterval)
        }
        }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            console.log('I was closed by the timer')
        }
        })
}

</script>
@endsection
