
@extends('cms.parent')

@section('title','Create Contact Request')
@section('page-name','Create Contact Request')
@section('main-page','Contact Request')
@section('sub-page','Create Contact Request')

@section('styles')

<!-- Toastr -->
<link rel="stylesheet" href="{{ asset('cms/plugins/toastr/toastr.min.css') }}">


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Admin</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="create_form"  >
                {{-- @csrf --}}
                <div class="card-body">

                <div class="form-grup">





                  <div class="form-group">
                    <label for="first_name">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter title"  >
                    {{-- minlength="3" --}}
                </div>






                  <div class="form-group">
                    <label for="first_name">Message</label>
                    <input type="text" class="form-control" id="message" name="message" placeholder="Enter message">
                  </div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="button" onclick="store()" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
 <!-- bs-custom-file-input -->
<script src="{{ asset('cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('cms/plugins/toastr/toastr.min.js') }}"></script>


    <script>
        //  $('#create_form').validate();
        // $('#create_form').submit(function(e){
        //     e.preventDefault();
function store(){
    var data = new FormData(document.getElementById('create_form'));

axios.post('{{ route('contacts.store') }}',data)
.then(function (response) {
    console.log(response);
    // document.getElementById('create_form').reset();
    showToaster(response.data.message, true);
})
.catch(function (error) {
    console.log(error.response);
    showToaster(error.response.data.message, false);
});
}


        function showToaster(message, status){
            if(status){
                toastr.success(message);
            }else{
                toastr.error(message);
            }
        }
    </script>

@endsection
