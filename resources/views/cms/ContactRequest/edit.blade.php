
@extends('cms.parent')

@section('title','Create Admin')
@section('page-name','Create Admin')
@section('main-page','Admins')
@section('sub-page','Create Admin')

@section('styles')

<!-- Toastr -->


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Admin</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="edit_form" action="{{ route('contacts.update' , $contact) }}" method="post" >
                @csrf
                @method('PUT')
                <div class="card-body">

                <div class="form-grup">





                  <div class="form-group">
                    <label for="first_name">Title</label>
                    <input type="text" class="form-control" id="title" value="{{ $contact->title }}" name="title" placeholder="Enter title"  required>
                    {{-- minlength="3" --}}
                </div>

                  <div class="form-group">
                    <label for="email">Message</label>
                    <input type="text" class="form-control" id="message" name="message" value="{{ $contact->message }}" placeholder="Enter Message">
                  </div>



                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
 <!-- bs-custom-file-input -->
<script src="{{ asset('cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<!-- Toastr -->


    <script>
        //  $('#create_form').validate();
        $('#edit_form').submit(function(e){
            e.preventDefault();

            var data = new FormData(this);

            axios.post('{{ route('contacts.update',$contact) }}',data)
            .then(function (response) {
                console.log(response);
                showToaster(response.data.message, true);
            })
            .catch(function (error) {
                console.log(error.response);
                showToaster(error.response.data.message, false);
            });
        });


    </script>

@endsection
