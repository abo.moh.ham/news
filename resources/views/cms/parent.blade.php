<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>News System | @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('cms/plugins/select2/css/select2.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('cms/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('cms/dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('cms/plugins/toastr/toastr.min.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        label.error{
            color:red;
        }
    </style>


    @yield('styles')
<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="../../index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="#" class="nav-link">Contact</a>
        </li>
      </ul>

      <!-- SEARCH FORM -->
      <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-comments"></i>
            <span class="badge badge-danger navbar-badge">3</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="{{ asset('cms/dist/img/user1-128x128.jpg') }}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Brad Diesel
                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">Call me whenever you can...</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="{{ asset('cms/dist/img/user8-128x128.jpg') }}" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    John Pierce
                    <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">I got your message bro</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="{{ asset('cms/dist/img/user3-128x128.jpg') }}" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Nora Silvester
                    <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">The subject goes here</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
            <i class="fas fa-th-large"></i>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="../../index3.html" class="brand-link">
        <img src="{{ asset('cms/dist/img/AdminLTELogo.png') }}"
             alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="{{ asset('cms/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">{{ Auth::user()->name }}</a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            @if (auth('author')->check())
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item ">
                  <a href="{{ route('cms.dashboard') }}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                      Dashboard
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>

                </li>






                  <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                      <i class="nav-icon far fa-envelope"></i>
                      <p>
                        Articles
                        <i class="fas fa-angle-left right"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="{{ route('articles.index') }}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Index</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{ route('articles.create') }}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Create</p>
                        </a>
                      </li>

                    </ul>
                  </li>


                  <li class="nav-item">
                      <a href="{{ route('auth-author.logout') }}" class="nav-link">
                          <i class="nav-icon fas fa-sign-out-alt"></i>
                          <p>Logout</p>
                      </a>
               </li>

              </ul>
@else
<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
    <li class="nav-item ">
      <a href="{{ route('cms.dashboard') }}" class="nav-link">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Dashboard
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>

    </li>
    <li class="nav-item">
      <a href="../widgets.html" class="nav-link">
        <i class="nav-icon fas fa-th"></i>
        <p>
          Widgets
          <span class="right badge badge-danger">New</span>
        </p>
      </a>
    </li>
    <li class="nav-header">content mangment</li>
@canany('read-admins','	create-admins')
    <li class="nav-item has-treeview">
      <a href="#" class="nav-link">

        <i class="nav-icon far fa-envelope"></i>
        <p>
          Admin
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
          @can('read-admins')
          <li class="nav-item">
            <a href="{{ route('admins.index') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Index</p>
            </a>
          </li>
          @endcan

        @can('	create-admins')
        <li class="nav-item">
            <a href="{{ route('admins.create') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Create</p>
            </a>
          </li>
        @endcan

      </ul>
    </li>
@endcanany

@canany('create-users','read-users')
    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
          <i class="nav-icon far fa-envelope"></i>
          <p>
           User
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
@can('read-users')
<li class="nav-item">
  <a href="{{ route('users.index') }}" class="nav-link">
    <i class="far fa-circle nav-icon"></i>
    <p>Index</p>
  </a>
</li>
@endcan
@can('create-users')
<li class="nav-item">
  <a href="{{ route('users.create') }}" class="nav-link">
    <i class="far fa-circle nav-icon"></i>
    <p>Create</p>
  </a>
</li>
@endcan


        </ul>
    </li>
@endcanany


@canany('create-contact_requests','read-contact_requests')
    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
          <i class="nav-icon far fa-envelope"></i>
          <p>
            Contact Request
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
            @can('read-contact_requests')
            <li class="nav-item">
                <a href="{{ route('contacts.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Index</p>
                </a>
              </li>
            @endcan
            @can('create-contact_requests')
            <li class="nav-item">
                <a href="{{ route('contacts.create') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Create</p>
                </a>
            </li>
            @endcan


        </ul>
      </li>
@endcanany

@canany('create-comments','read-comments')
<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
      <i class="nav-icon far fa-envelope"></i>
      <p>
        Comment
        <i class="fas fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        @can('read-comments')
        <li class="nav-item">
            <a href="{{ route('comments.index') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Index</p>
            </a>
          </li>
        @endcan
        @can('create-comments')
        <li class="nav-item">
            <a href="{{ route('comments.create') }}" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Create</p>
            </a>
        </li>
        @endcan


    </ul>
  </li>
        @endcanany


        @canany('create-authors','read-authors')
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="nav-icon far fa-envelope"></i>
            <p>
                Authors
                <i class="fas fa-angle-left right"></i>
            </p>
            </a>
            <ul class="nav nav-treeview">
                @can('read-authors')
                <li class="nav-item">
                    <a href="{{ route('authors.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Index</p>
                    </a>
                </li>
                @endcan
                @can('create-authors')
                <li class="nav-item">
                    <a href="{{ route('authors.create') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Create</p>
                    </a>
                </li>
                @endcan



            </ul>
        </li>
        @endcanany


        @canany('create-categories','read-categories')

      <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
          <i class="nav-icon far fa-envelope"></i>
          <p>
            Categories
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
            @can('read-categories')
            <li class="nav-item">
                <a href="{{ route('categories.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Index</p>
                </a>
              </li>
            @endcan
            @can('create-categories')
            <li class="nav-item">
                <a href="{{ route('categories.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create</p>
                </a>
              </li>
            @endcan


        </ul>
      </li>
        @endcanany

@canany('create-articles','read-articles')
<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
      <i class="nav-icon far fa-envelope"></i>
      <p>
        Articles
        <i class="fas fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        @can('read-articles')
        <li class="nav-item">
            <a href="{{ route('articles.index') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Index</p>
            </a>
          </li>
        @endcan
        @can('create-articles')
        <li class="nav-item">
            <a href="{{ route('articles.create') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Create</p>
            </a>
          </li>
        @endcan


    </ul>
  </li>

@endcanany



    <li class="nav-header">EXAMPLES</li>
@can('read-articles')
<li class="nav-item">
    <a href="{{ route('settings.index') }}" class="nav-link">
      <i class="nav-icon far fa-image"></i>
      <p>
          Settings
      </p>
    </a>
  </li>
@endcan

    <li class="nav-item has-treeview">
      <a href="#" class="nav-link">
        <i class="nav-icon far fa-envelope"></i>
        <p>
          Mailbox
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="../mailbox/mailbox.html" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Inbox</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="../mailbox/compose.html" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Compose</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="../mailbox/read-mail.html" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Read</p>
          </a>
        </li>
      </ul>
    </li>


    @canany(['create-roles','read-roles','create-prmissions','read-permissions'])
    <li class="nav-header">Roles & Permissions</li>
    @endcanany

    @canany(['create-roles','read-roles'])
    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-pencil-ruler"></i>
            <p>
                Roles
                <i class="fas fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('read-roles')
            <li class="nav-item">
                <a href="{{ route('roles.index') }}" class="nav-link">
                    <i class="fas fa-list nav-icon"></i>
                    <p>Index</p>
                </a>
            </li>
            @endcan
            @can('create-roles')
            <li class="nav-item">
                <a href="{{ route('roles.create') }}" class="nav-link">
                    <i class="fas fa-plus nav-icon"></i>
                    <p>Create</p>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcanany

    @canany(['create-permissions','read-permissions'])
    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-user-tie"></i>
            <p>
                Permissions
                <i class="fas fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('read-permissions')
            <li class="nav-item">
                <a href="{{ route('permissions.index') }}" class="nav-link">
                    <i class="fas fa-list nav-icon"></i>
                    <p>Index</p>
                </a>
            </li>
            @endcan
            @can('create-permissions')
            <li class="nav-item">
                <a href="{{ route('permissions.create') }}" class="nav-link">
                    <i class="fas fa-plus nav-icon"></i>
                    <p>Create</p>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcanany








    <li class="nav-item">
        <a href="{{ route('auth.edit-password') }}" class="nav-link">
            <i class="nav-icon fas fa-lock"></i>
            <p>
                Change Password
            </p>
        </a>
    </li>

@if (auth('admin')->check())
<li class="nav-item">
    <a href="{{ route('auth.logout') }}" class="nav-link">
        <i class="nav-icon fas fa-sign-out-alt"></i>
        <p>Logout</p>
    </a>

@else
<li class="nav-item">
    <a href="{{ route('auth.logout') }}" class="nav-link">
        <i class="nav-icon fas fa-sign-out-alt"></i>
        <p>Logout</p>
    </a>

@endif



    </li>

  </ul>
            @endif

        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@yield('page-name')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">@yield('main-page')</a></li>
                            <li class="breadcrumb-item active">@yield('sub-page')</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        @yield('content')
        <!-- /.content -->
    </div>


    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.5
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->>
  <script src="{{ asset('js/crud.js') }}"></script>

    <!-- jQuery -->
    <script src="{{ asset('cms/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('cms/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    {{-- <script src="{{ asset('cms/plugins/jquery-validation/localization/messages_ar.js') }}"></script> --}}
    <script src="{{ asset('cms/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('cms/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('cms/dist/js/demo.js') }}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script> --}}
    <script src="{{ asset('js/sweetalert.js') }}"></script>
    <script src="{{ asset('cms/plugins/toastr/toastr.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('cms/plugins/select2/js/select2.full.min.js') }}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script> --}}
<script src="{{ asset('Js/axios.js') }}"> </script>

<script>
    @if(session()->has('message'))
    showToaster('{{ session()->get('message') }}','success');
    @endif

    function showToaster(message, status){
            if(status){
                toastr.success(message);
            }else{
                toastr.error(message);
            }
        }
        $('.select').select2({
        })
</script>
    @yield('scripts')
</body>

</html>
