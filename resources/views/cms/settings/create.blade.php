
@extends('cms.parent')

@section('title','Create Settings')
@section('page-name','Create Settings')
@section('main-page','Settings')
@section('sub-page','Create Settings')

@section('styles')

<!-- Toastr -->
<link rel="stylesheet" href="{{ asset('cms/plugins/toastr/toastr.min.css') }}">


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="create_form" action="{{ route('settings.store') }}" method="post" >
                @csrf
                <div class="card-body">

                <div class="form-grup">







                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email">
                  </div>
                  <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="Enter mobile">
                  </div>

                  <div class="form-group">
                    <label for="mobile">Phone</label>
                    <input type="tel" class="form-control" id="phone" name="phone" placeholder="Enter phone">
                  </div>



                  <div class="form-group">
                    <label for="first_name">working_time</label>
                    <input type="text" class="form-control" id="working_time" name="working_time" placeholder="Enter working time">
                  </div>

                  <div class="form-group">
                    <label for="first_name">box_office</label>
                    <input type="text" class="form-control" id="box_office" name="box_office" placeholder="Enter box_office">
                  </div>
                  <div class="form-group">
                    <label for="first_name">adress</label>
                    <input type="text" class="form-control" id="adress" name="adress" placeholder="Enter adress">
                  </div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
 <!-- bs-custom-file-input -->
<script src="{{ asset('cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('cms/plugins/toastr/toastr.min.js') }}"></script>


    <script>
        //  $('#create_form').validate();
        $('#create_form').submit(function(e){
            e.preventDefault();

            var data = new FormData(this);

            axios.post('{{ route('settings.store') }}',data)
            .then(function (response) {
                console.log(response);
                document.getElementById('create_form').reset();
                showToaster(response.data.message, true);
            })
            .catch(function (error) {
                console.log(error.response);
                showToaster(error.response.data.message, false);
            });
        });

        function showToaster(message, status){
            if(status){
                toastr.success(message);
            }else{
                toastr.error(message);
            }
        }
    </script>

@endsection
