
@extends('cms.parent')

@section('title','Create Comment')
@section('page-name','Create Comment')
@section('main-page','Comments')
@section('sub-page','Create Comment')

@section('styles')

<!-- Toastr -->
<link rel="stylesheet" href="{{ asset('cms/plugins/toastr/toastr.min.css') }}">


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Coomment</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="create_form" action="{{ route('comments.store') }}" method="post" >
                @csrf
                <div class="card-body">

                <div class="form-grup">





                  <div class="form-group">
                    <label for="first_name">Comment</label>
                <textarea name="comment" id="comment" cols="30" rows="10" class="form-control" placeholder="Enter comment"  required></textarea>
                </div>
                <div class="form-group">
                    <label for="first_name">Article</label>
                    <select class="form-control select" name="article_id" id="article_id" >
                        @foreach ( $articles as $article )
                        <option value="{{ $article->id }}">{{ $article->title }}</option>

                        @endforeach
                    </select>
                </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
 <!-- bs-custom-file-input -->
<script src="{{ asset('cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('cms/plugins/toastr/toastr.min.js') }}"></script>


    <script>
        //  $('#create_form').validate();
        $('#create_form').submit(function(e){
            e.preventDefault();

            var data = new FormData(this);

            axios.post('{{ route('comments.store') }}',data)
            .then(function (response) {
                console.log(response);
                document.getElementById('create_form').reset();
                showToaster(response.data.message, true);
            })
            .catch(function (error) {
                console.log(error.response);
                showToaster(error.response.data.message, false);
            });
        });

        function showToaster(message, status){
            if(status){
                toastr.success(message);
            }else{
                toastr.error(message);
            }
        }
    </script>

@endsection
