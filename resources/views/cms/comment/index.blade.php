@extends('cms.parent')

@section('title','Comments')
@section('page-name','Index Comments')
@section('main-page','Comments')
@section('sub-page','Index')

@section('styles')
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Comments</h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 50px;">
                                <input type="text" name="table_search" class="form-control float-right"
                                    placeholder="Search">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover table-bordered text-nowrap">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Comment</th>
                                    <th>user</th>
                                    <th>article</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Settings</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($comments as $comment)
                                {{-- {{ dd($comment) }} --}}
                                <tr>
                                    <td>{{ $comment->id }}</td>
                                    <td>{{ $comment->comment}}</td>
                                    <td><a href="{{ route('users.edit',$comment->user->id) }}">
                                        {{ $comment->user->name}}</a></td>
                                    <td><a href="{{ route('articles.edit',$comment->article->id) }}">{{ $comment->article->title}}</a></td>


                                    <td>{{ $comment->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $comment->updated_at->format('Y-m-d') }}</td>
                                    <td>
                                    <div class="btn-group">
                                            <a  href="{{ route('comments.edit',$comment->id) }}" type="button"
                                                class="btn btn-info"><i class="fas fa-edit"></i></a>
                                            <button onclick="performDestroy({{ $comment->id }},this)"
                                                  class="btn btn-danger"><i class="fas fa-trash"></i>
                                            </button>


                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        {{-- {{ $comments->links() }} --}}
                        {{-- {{ $comments->render }} --}}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.row -->
    </div>
</section>
@endsection

@section('scripts')
<script>
    function performDestroy(id, td){
        confirmDestroy('/cms/comments/'+id, td);

    }

</script>

 <script>

</script>
@endsection
