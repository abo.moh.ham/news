
@extends('cms.parent')

@section('title','Edit User')
@section('page-name','Edit User')
@section('main-page','Users')
@section('sub-page','Edit User ')

@section('styles')

<!-- Toastr -->


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="edit_form" action="{{ route('users.update' , $user) }}" method="post" >
                @csrf
                @method('PUT')
                <div class="card-body">

                <div class="form-grup">





                  <div class="form-group">
                    <label for="first_name">Name</label>
                    <input type="text" class="form-control" id="name" value="{{ $user->name }}" name="name" placeholder="Enter name"  required>
                    {{-- minlength="3" --}}
                </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" placeholder="Enter Email">
                  </div>
                  <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input type="tel" class="form-control" id="mobile_number" name="mobile_number" value="{{$user->mobile_number}}" placeholder="Enter Mobile">
                  </div>



                  <div class="form-group">
                    <label for="first_name">Status</label>
                    <input type="text" class="form-control" id="status" name="status" value="{{ $user->status }}" placeholder="Enter Status">
                  </div>

                  <div class="form-group">
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="male" value="M"  name="gender"
                      @if($user->gender == 'M') checked @endif
                      >
                      <label for="male" class="custom-control-label">Male</label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="female" value="F" name="gender"
                      @if($user->gender == 'F') checked @endif
                      >
                      <label for="female" class="custom-control-label">Female</label>
                    </div>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
 <!-- bs-custom-file-input -->
<script src="{{ asset('cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<!-- Toastr -->


    <script>
        //  $('#create_form').validate();
        $('#edit_form').submit(function(e){
            e.preventDefault();

            var data = new FormData(this);

            axios.post('{{ route('users.update',$user) }}',data)
            .then(function (response) {
                console.log(response);
                showToaster(response.data.message, true);
            })
            .catch(function (error) {
                console.log(error.response);
                showToaster(error.response.data.message, false);
            });
        });


    </script>

@endsection
