@extends('user.index.parent')




@section('content')



<section class="inner-blogs wow fadeIn">
    <div class="blogs-section-title text-center">
        <h2 class="wow fadeInDown">صفحة الكاتب</h2>
        <p class="wow fadeInDown">{{ $author->name }}</p>
    </div>
    {{-- <div class="big-items-wrap container">
        <div class="big-items row">
            @foreach ($articlesSpecial as $article )
            <div class="col-12 {{ $loop->first?"col-lg-8":"col-lg-4" }}">
                <div class="big-blog-item wow fadeInDown">
                    <img src="{{ asset($article->image->image_url   ) }}" class="blog-img"/>
                    <a href="{{ route('home-article',[$article->id]) }}" class="blog-item-link">
                        <div class="blog-content">
                            <object>
                                <a href="#" class="category">{{ $article->category->getName() }}</a>
                            </object>
                            <h3>{{ $article->title }}</h3>
                            <object>
                                <a href="#" class="author">
                                    <img src="{{ asset('user/images/user.png') }}" />
                                    <span>{{ $article->author->name }}</span>
                                </a>
                            </object>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach


        </div>
    </div> --}}
    <div class="other-blogs-wrap">
        <div class="container">
            <div class="row blog-items-row">
                @foreach ($articales as $articale )
                <div class="col-6 col-sm-6 col-md-4 col-lg-3">
                    <div class="blog-item wow fadeInDown">
                        <div class="blog-img">
                            <a href="{{ route('home-article',[$articale->id]) }}">
                                <img src="{{ asset($articale->image->image_url) }}" />
                            </a>
                        </div>
                        <div class="blog-content">
                            <a href="#" class="author-img">
                                <img src="{{ asset('user/images/user.png') }}" />
                            </a>
                            <div class="blog-meta">
                                <a href="{{ route('home-author',[$articale->author->id]) }}" class="author">{{ $articale->author->name }}</a>
                                <a href="{{ route('home-category',[$articale->category->id]) }}" class="category">{{ $articale->category->name }}</a>
                            </div>
                            <h3><a href="#">{{ $articale->title }}</a></h3>
                        </div>
                    </div>
                </div>
                @endforeach


            </div>
            {{ $articales->links('pagination.news') }}
        </div>
    </div>
</section>
@endsection







