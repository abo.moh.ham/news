<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1">
    <title>عصارة</title>
    <link rel="stylesheet" href="{{ asset('cms/plugins/fontawesome-free/css/all.min.css') }}">

    <link rel="icon" href="{{ asset('user/images/favicon.png') }}" type="image/png">
    <link href="{{ asset('user/css/bootstrap-rtl.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('user/css/animate.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('user/css/jquery.mCustomScrollbar.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('user/css/owl.carousel.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('user/css/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('user/css/intlTelInput.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('user/css/nouislider.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('user/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('user/css/responsive.css') }}" rel="stylesheet" type="text/css">
@yield('styles')
</head>
<header>
    <div class="main-menu">
        <div class="container">
            <div class="main-menu-wrap">
                <ul>
                    <li><a href="#">الرئيسية</a></li>
                    <li class="drop-item">
                        <a href="#">
                            التصنيفات
                            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10">
                                <path id="Shape" d="M1.811,5,5.78,8.781a.69.69,0,0,1,0,1.01.777.777,0,0,1-1.061,0L.22,5.505a.69.69,0,0,1,0-1.01L4.72.209a.777.777,0,0,1,1.061,0,.69.69,0,0,1,0,1.01Z" transform="translate(0 0)"/>
                            </svg>
                        </a>
                        <div class="drop-menu">
                            <ul class="scrollable-area">
                                <li><a href="#">خيار أول</a></li>
                                <li><a href="#">خيار أول</a></li>
                                <li><a href="#">خيار ثالث</a></li>
                                <li><a href="#">خيار ثالث</a></li>
                                <li><a href="#">خيار أول</a></li>
                                <li><a href="#">خيار الرابع</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="#">المدربين</a></li>
                    <li><a href="#">البودكاست</a></li>
                    <li><a href="#">المدونة</a></li>
                    <li><a href="#">من نحن</a></li>
                    <li><a href="#">الأسئلة الشائعة</a></li>
                    <li><a href="#">التسويق بالعمولة</a></li>
                </ul>
                <a href="#" class="close-menu">
                    <svg id="Combined_Shape" data-name="Combined Shape" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12">
                        <path id="Combined_Shape-2" data-name="Combined Shape" d="M10.537,11.749,6,7.212,1.463,11.749A.857.857,0,0,1,.251,10.537L4.787,6,.251,1.463A.857.857,0,0,1,1.463.251L6,4.787,10.537.251a.857.857,0,0,1,1.212,1.213L7.212,6l4.537,4.537a.857.857,0,1,1-1.212,1.212Z" fill="#fff"/>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="header-wrap wow fadeInDown">
            <div class="main-header d-flex">
                <a href="{{ route('home') }}" class="logo">
                    <img src="{{ asset('user/images/colored-logo.svg') }}" width="140" />
                </a>
                <div class="header-items">
                    <div class="header-item header-item">
                        {{-- <a href="#" class="header-icon menu"> --}}
                            @if (auth('user')->check())
                            <a href="{{ route('auth-user.logout') }}" class="header-icon ">
                                logout
                                      </a>
                                      @else
                                <a href="{{ route('auth-user.login.view') }}" class="header-icon ">
                             Sign In
                                   </a>
                                   @endif
                    </div>
                    <div class="header-item">
                        <a href="#" class="header-icon notifications dropdown-menu-toggle">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19.995" viewBox="0 0 20 19.995">
                                <path id="Shape-2" d="M473,371.818a5.461,5.461,0,0,1,5.455,5.455v4.545a3.618,3.618,0,0,0,.488,1.818H467.057a3.618,3.618,0,0,0,.488-1.818v-4.545A5.461,5.461,0,0,1,473,371.818M473,370a7.273,7.273,0,0,0-7.273,7.273v4.545a1.818,1.818,0,0,1-1.818,1.818.909.909,0,0,0,0,1.818h18.182a.909.909,0,1,0,0-1.818,1.818,1.818,0,0,1-1.818-1.818v-4.545A7.273,7.273,0,0,0,473,370Zm1.573,17.273h-3.145a.909.909,0,0,0-.786,1.364h0a2.727,2.727,0,0,0,4.718,0,.909.909,0,0,0-.787-1.364Z" transform="translate(-463 -370)"/>
                            </svg>
                            <span class="badge">3</span>
                        </a>
                        <div class="notifications-menu dropdown-menu-item">
                            <div class="notf-items scrollable-area">
                                <div class="notf-item">
                                    <a href="#">
                                        <img src="{{ asset('user/images/notf-img.png') }}" />
                                        <div class="notf-content">
                                            <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                                            <span>منذ عشر دقائق</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="notf-item">
                                    <a href="#">
                                        <img src="images/notf-img.png" />
                                        <div class="notf-content">
                                            <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                                            <span>منذ عشر دقائق</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="notf-item">
                                    <a href="#">
                                        <img src="{{ asset('user/images/notf-img.png') }}" />
                                        <div class="notf-content">
                                            <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                                            <span>منذ عشر دقائق</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="notf-item">
                                    <a href="#">
                                        <img src="{{ asset('user/images/notf-img.png') }}" />
                                        <div class="notf-content">
                                            <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                                            <span>منذ عشر دقائق</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="notf-item">
                                    <a href="#">
                                        <img src="{{ asset('user/images/notf-img.png') }}" />
                                        <div class="notf-content">
                                            <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                                            <span>منذ عشر دقائق</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="notf-item">
                                    <a href="#">
                                        <img src="{{ asset('user/images/notf-img.png') }}" />
                                        <div class="notf-content">
                                            <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                                            <span>منذ عشر دقائق</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="notf-item">
                                    <a href="#">
                                        <img src="{{ asset('user/images/notf-img.png') }}" />
                                        <div class="notf-content">
                                            <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                                            <span>منذ عشر دقائق</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-item">
                        <a href="#" class="header-icon search">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                <path id="Combined_Shape" data-name="Combined Shape" d="M18.292,19.707l-3.678-3.678a9.009,9.009,0,1,1,1.414-1.414l3.678,3.678a1,1,0,0,1-1.415,1.415ZM2,9a7,7,0,0,0,11.87,5.024,1,1,0,0,1,.154-.154A7,7,0,1,0,2,9Z"/>
                            </svg>
                      

                        </a>

                    </div>
                    <div class="header-item">
                        <a href="#" class="header-icon profile">
                            <img src="{{ asset('user/images/user.png') }}" />
                        </a>
                    </div>
                    <div class="header-item menu-header-item">
                        <a href="#" class="header-icon menu">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="8" viewBox="0 0 15 8">
                                <path id="Combined_Shape" data-name="Combined Shape" d="M1,8A1,1,0,0,1,1,6H14a1,1,0,0,1,0,2ZM1,2A1,1,0,0,1,1,0H14a1,1,0,0,1,0,2Z" fill="#fff"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="mobile-menu">
    <div class="mobile-ul">
        <a href="#" class="menu-logo"><img src="{{ asset('user/images/logo-white.svg') }}" /></a>
        <div class="mobile-ul-wrap scrollable-area">
            <ul>
                <li><a href="#">الرئيسية</a></li>
                <li class="drop-item">
                    <a href="#">
                        التصنيفات
                        <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10">
                            <path id="Shape" d="M1.811,5,5.78,8.781a.69.69,0,0,1,0,1.01.777.777,0,0,1-1.061,0L.22,5.505a.69.69,0,0,1,0-1.01L4.72.209a.777.777,0,0,1,1.061,0,.69.69,0,0,1,0,1.01Z" transform="translate(0 0)"/>
                        </svg>
                    </a>
                    <div class="drop-menu">
                        <ul>
                            <li><a href="#">خيار أول</a></li>
                            <li><a href="#">خيار أول</a></li>
                            <li><a href="#">خيار ثالث</a></li>
                            <li><a href="#">خيار ثالث</a></li>
                            <li><a href="#">خيار أول</a></li>
                            <li><a href="#">خيار الرابع</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="#">المدربين</a></li>
                <li><a href="#">البودكاست</a></li>
                <li><a href="#">المدونة</a></li>
                <li><a href="#">من نحن</a></li>
                <li><a href="#">الأسئلة الشائعة</a></li>
                <li><a href="#">التسويق بالعمولة</a></li>
            </ul>
        </div>
    </div>
    <div class="mobile-icons">
        <a href="#" class="mobile-menu-close">
            <svg id="Combined_Shape" data-name="Combined Shape" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                <path id="Combined_Shape-2" data-name="Combined Shape" d="M8.781,9.79,5,6.01,1.219,9.79A.714.714,0,0,1,.209,8.781L3.99,5,.209,1.219A.715.715,0,0,1,1.219.209L5,3.99,8.781.209A.714.714,0,0,1,9.79,1.219L6.01,5,9.79,8.781a.714.714,0,1,1-1.01,1.01Z" fill="#fff"/>
            </svg>
        </a>
        <div class="mobile-icons-wrapper">
            <div class="mobile-menu-icons">
                <div class="header-item">
                    <a href="#" class="header-icon cart">
                        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20.164" viewBox="0 0 22 20.164">
                            <path id="shopping-cart" d="M21.1,25.009H3.895l-.256-2.848a.9.9,0,0,0-.9-.823H.9a.9.9,0,1,0,0,1.807h1.01L2.952,34.722a3.476,3.476,0,0,0,1.3,2.355,2.738,2.738,0,1,0,4.733.753h4.945a2.739,2.739,0,1,0,2.576-1.807H6.417a1.678,1.678,0,0,1-1.543-1.017l14.439-.849a.9.9,0,0,0,.824-.683l1.836-7.343A.9.9,0,0,0,21.1,25.009ZM6.411,39.695a.932.932,0,1,1,.932-.932A.933.933,0,0,1,6.411,39.695Zm10.1,0a.932.932,0,1,1,.932-.932A.933.933,0,0,1,16.507,39.695Zm2.038-7.3L4.631,33.21l-.574-6.394H19.939Z" transform="translate(0 -21.338)"/>
                        </svg>
                        <span class="badge">3</span>
                    </a>
                </div>
                <div class="header-item">
                    <a href="#" class="header-icon profile">
                        <img src="{{ asset('user/images/user.png') }}" />
                    </a>
                </div>
                <div class="mobile-social">
                    <ul class="social-ul">
                        <li><a href="#"><svg id="Component_64_1" data-name="Component 64 – 1" xmlns="http://www.w3.org/2000/svg" width="24.5" height="18.683" viewBox="0 0 24.5 18.683"><path id="Path_725" data-name="Path 725" d="M558.786,378.8l-5.249-2.872a1.214,1.214,0,0,0-1.8,1.065v5.7a1.2,1.2,0,0,0,.354.859,1.3,1.3,0,0,0,.86.357,1.218,1.218,0,0,0,.575-.146l5.249-2.822a1.212,1.212,0,0,0,.008-2.14Zm-5.154,2.755V378.14l3.15,1.723Z" transform="translate(-542.751 -370.75)"/><path id="Path_726" data-name="Path 726" d="M567.073,375.264l-.038,0,.037-.005a5.753,5.753,0,0,0-1.125-2.935,4.141,4.141,0,0,0-2.74-1.29l-.171-.014c-3.177-.235-7.991-.268-8.043-.268s-4.847.032-8.041.262l-.168.018a4.046,4.046,0,0,0-2.758,1.354,6,6,0,0,0-1.095,2.878c-.018.158-.18,2.073-.18,3.943V381c0,1.983.177,3.924.178,3.949a5.681,5.681,0,0,0,1.124,2.9,4.2,4.2,0,0,0,2.824,1.274c.093.011.17.019.232.031l.067.009c1.71.162,6.806.249,7.857.265.044,0,4.845-.032,8.012-.262l.191-.016a3.953,3.953,0,0,0,2.738-1.323,5.991,5.991,0,0,0,1.094-2.875c.015-.124.183-2.062.183-3.957v-1.8C567.251,377.4,567.1,375.614,567.073,375.264Zm-1.9,9.486a4.238,4.238,0,0,1-.628,1.809,2.093,2.093,0,0,1-1.5.688l-.14.016c-3.112.225-7.829.256-7.874.256-.243,0-5.913-.092-7.633-.251-.095-.016-.2-.028-.33-.042a2.371,2.371,0,0,1-1.61-.655,3.818,3.818,0,0,1-.634-1.811c-.007-.078-.169-1.938-.169-3.761v-1.8c0-1.81.162-3.677.169-3.752a4.129,4.129,0,0,1,.627-1.806,2.2,2.2,0,0,1,1.538-.72l.1-.012c3.16-.227,7.861-.257,7.907-.257h0c.077,0,4.73.029,7.864.255l.143.017a2.263,2.263,0,0,1,1.53.674,3.877,3.877,0,0,1,.635,1.842c0,.018.169,1.921.169,3.761V381C565.344,382.853,565.177,384.736,565.175,384.75Z" transform="translate(-542.751 -370.75)"/></svg></a></li>
                        <li><a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24.004" height="19.973" viewBox="0 0 24.004 19.973"> <path id="Path_728" data-name="Path 728" d="M496.932,387.973h0A11.391,11.391,0,0,0,507.7,380.75a14.012,14.012,0,0,0,.963-5.234c0-.1-.006-.21-.016-.318l-.077-.869.584-.647c.121-.134.235-.272.344-.415l-.014.006-1.127.413-.895-.8a3.481,3.481,0,0,0-5.8,2.618l.015,2.17-2.164-.163a11.607,11.607,0,0,1-7.447-3.456,9.388,9.388,0,0,0,.105,5.085,7.706,7.706,0,0,0,3.455,4.2L498.8,385.3l-3.391,1.562a12.639,12.639,0,0,1-1.817.672,13.161,13.161,0,0,0,3.336.441m0,2a15.956,15.956,0,0,1-7.756-2.088,1,1,0,0,1,.48-1.875h.046c.142.006.283.008.425.008a10.644,10.644,0,0,0,4.449-.975,9.644,9.644,0,0,1-4.308-5.285,11.665,11.665,0,0,1-.015-6.7,10.652,10.652,0,0,1,.495-1.456,1,1,0,0,1,1.732-.168,9.66,9.66,0,0,0,7.181,4.081,5.48,5.48,0,0,1,9.133-4.122,9.9,9.9,0,0,0,2.291-1.2.981.981,0,0,1,.573-.187,1,1,0,0,1,.975,1.24,8.709,8.709,0,0,1-1.995,3.774c.015.162.023.325.024.49C510.662,384.34,504.317,389.973,496.932,389.973Z" transform="translate(-488.66 -370)"/> </svg></a></li>
                        <li><a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="13.001" height="22" viewBox="0 0 13.001 22"> <path id="Path_727" data-name="Path 727" d="M446.021,389v-8h3.219l.5-2h-3.719v-6h4v-2h-2a4.005,4.005,0,0,0-4,4v4h-3v2h3v8h2m1,2h-4a1,1,0,0,1-1-1v-7h-2a1,1,0,0,1-1-1v-4a1,1,0,0,1,1-1h2v-2a6,6,0,0,1,6-6h3a1,1,0,0,1,1,1v4a1,1,0,0,1-1,1h-3v2h3a1,1,0,0,1,.97,1.242l-1,4a1,1,0,0,1-.97.757h-2v7A1,1,0,0,1,447.021,391Z" transform="translate(-439.021 -369)" /> </svg></a></li>
                        <li><a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22"> <path id="Combined_Shape-2" data-name="Combined Shape-2" d="M398,391H388a6.007,6.007,0,0,1-6-6V375a6.007,6.007,0,0,1,6-6h10a6.007,6.007,0,0,1,6,6v10A6.006,6.006,0,0,1,398,391Zm-10-20a4.005,4.005,0,0,0-4,4v10a4,4,0,0,0,4,4h10a4,4,0,0,0,4-4V375a4,4,0,0,0-4-4Zm5.044,13.957a5,5,0,1,1,2.251-.534,5,5,0,0,1-2.251.534Zm0-8a3,3,0,1,0,3,3,3,3,0,0,0-2.561-2.968,3.09,3.09,0,0,0-.441-.032Z" transform="translate(-382 -369)"/> </svg></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mobile-action-bar">
    <div class="header-item actions-item">
        <a href="#" class="header-icon search">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                <path id="Combined_Shape" data-name="Combined Shape" d="M18.292,19.707l-3.678-3.678a9.009,9.009,0,1,1,1.414-1.414l3.678,3.678a1,1,0,0,1-1.415,1.415ZM2,9a7,7,0,0,0,11.87,5.024,1,1,0,0,1,.154-.154A7,7,0,1,0,2,9Z"/>
            </svg>
        </a>
    </div>
    <div class="header-item actions-item">
        <a href="#" class="header-icon cart">
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20.164" viewBox="0 0 22 20.164">
                <path id="shopping-cart" d="M21.1,25.009H3.895l-.256-2.848a.9.9,0,0,0-.9-.823H.9a.9.9,0,1,0,0,1.807h1.01L2.952,34.722a3.476,3.476,0,0,0,1.3,2.355,2.738,2.738,0,1,0,4.733.753h4.945a2.739,2.739,0,1,0,2.576-1.807H6.417a1.678,1.678,0,0,1-1.543-1.017l14.439-.849a.9.9,0,0,0,.824-.683l1.836-7.343A.9.9,0,0,0,21.1,25.009ZM6.411,39.695a.932.932,0,1,1,.932-.932A.933.933,0,0,1,6.411,39.695Zm10.1,0a.932.932,0,1,1,.932-.932A.933.933,0,0,1,16.507,39.695Zm2.038-7.3L4.631,33.21l-.574-6.394H19.939Z" transform="translate(0 -21.338)"/>
            </svg>
            <span class="badge">3</span>
        </a>
    </div>
    <div class="header-item actions-item">
        <a href="#" class="header-icon mobile-notifications-toggle">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19.995" viewBox="0 0 20 19.995">
                <path id="Shape-2" d="M473,371.818a5.461,5.461,0,0,1,5.455,5.455v4.545a3.618,3.618,0,0,0,.488,1.818H467.057a3.618,3.618,0,0,0,.488-1.818v-4.545A5.461,5.461,0,0,1,473,371.818M473,370a7.273,7.273,0,0,0-7.273,7.273v4.545a1.818,1.818,0,0,1-1.818,1.818.909.909,0,0,0,0,1.818h18.182a.909.909,0,1,0,0-1.818,1.818,1.818,0,0,1-1.818-1.818v-4.545A7.273,7.273,0,0,0,473,370Zm1.573,17.273h-3.145a.909.909,0,0,0-.786,1.364h0a2.727,2.727,0,0,0,4.718,0,.909.909,0,0,0-.787-1.364Z" transform="translate(-463 -370)"/>
            </svg>
            <span class="badge">3</span>
        </a>
    </div>
    <div class="header-item actions-item">
        <a href="#" class="header-icon profile">
            <img src="{{ asset('user/images/user.png') }}" />
        </a>
    </div>
</div>
<div class="notifications-menu dropdown-menu-item mobile-notifications">
    <div class="notifications-title">
        <h3>اشعارات</h3>
        <a href="#" class="notifications-close">
            <svg id="Combined_Shape" data-name="Combined Shape" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                <path id="Combined_Shape-2" data-name="Combined Shape" d="M8.781,9.79,5,6.01,1.219,9.79A.714.714,0,0,1,.209,8.781L3.99,5,.209,1.219A.715.715,0,0,1,1.219.209L5,3.99,8.781.209A.714.714,0,0,1,9.79,1.219L6.01,5,9.79,8.781a.714.714,0,1,1-1.01,1.01Z" fill="#fff"/>
            </svg>
        </a>
    </div>
    <div class="notf-items scrollable-area">
        <div class="notf-item">
            <a href="#">
                <img src="{{ asset('user/images/notf-img.png') }}" />
                <div class="notf-content">
                    <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                    <span>منذ عشر دقائق</span>
                </div>
            </a>
        </div>
        <div class="notf-item">
            <a href="#">
                <img src="{{ asset('user/images/notf-img.png') }}" />
                <div class="notf-content">
                    <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                    <span>منذ عشر دقائق</span>
                </div>
            </a>
        </div>
        <div class="notf-item">
            <a href="#">
                <img src="{{ asset('user/images/notf-img.png') }}" />
                <div class="notf-content">
                    <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                    <span>منذ عشر دقائق</span>
                </div>
            </a>
        </div>
        <div class="notf-item">
            <a href="#">
                <img src="{{ asset('user/images/notf-img.png') }}" />
                <div class="notf-content">
                    <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                    <span>منذ عشر دقائق</span>
                </div>
            </a>
        </div>
        <div class="notf-item">
            <a href="#">
                <img src="{{ asset('user/images/notf-img.png') }}" />
                <div class="notf-content">
                    <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                    <span>منذ عشر دقائق</span>
                </div>
            </a>
        </div>
        <div class="notf-item">
            <a href="#">
                <img src="{{ asset('user/images/notf-img.png') }}" />
                <div class="notf-content">
                    <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                    <span>منذ عشر دقائق</span>
                </div>
            </a>
        </div>
        <div class="notf-item">
            <a href="#">
                <img src="{{ asset('user/images/notf-img.png') }}" />
                <div class="notf-content">
                    <h3>لقد قام المدرب محمد عبد الرحمن بإضافة دورة جديدة</h3>
                    <span>منذ عشر دقائق</span>
                </div>
            </a>
        </div>
    </div>
</div>

@yield('content')
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="footer-logo-wrap wow fadeInDown">
                    <img src="{{ asset('user/images/colored-logo.svg') }}" />
                    <ul class="social-ul">
                        <li><a href="#"><svg id="Component_64_1" data-name="Component 64 – 1" xmlns="http://www.w3.org/2000/svg" width="24.5" height="18.683" viewBox="0 0 24.5 18.683"><path id="Path_725" data-name="Path 725" d="M558.786,378.8l-5.249-2.872a1.214,1.214,0,0,0-1.8,1.065v5.7a1.2,1.2,0,0,0,.354.859,1.3,1.3,0,0,0,.86.357,1.218,1.218,0,0,0,.575-.146l5.249-2.822a1.212,1.212,0,0,0,.008-2.14Zm-5.154,2.755V378.14l3.15,1.723Z" transform="translate(-542.751 -370.75)"/><path id="Path_726" data-name="Path 726" d="M567.073,375.264l-.038,0,.037-.005a5.753,5.753,0,0,0-1.125-2.935,4.141,4.141,0,0,0-2.74-1.29l-.171-.014c-3.177-.235-7.991-.268-8.043-.268s-4.847.032-8.041.262l-.168.018a4.046,4.046,0,0,0-2.758,1.354,6,6,0,0,0-1.095,2.878c-.018.158-.18,2.073-.18,3.943V381c0,1.983.177,3.924.178,3.949a5.681,5.681,0,0,0,1.124,2.9,4.2,4.2,0,0,0,2.824,1.274c.093.011.17.019.232.031l.067.009c1.71.162,6.806.249,7.857.265.044,0,4.845-.032,8.012-.262l.191-.016a3.953,3.953,0,0,0,2.738-1.323,5.991,5.991,0,0,0,1.094-2.875c.015-.124.183-2.062.183-3.957v-1.8C567.251,377.4,567.1,375.614,567.073,375.264Zm-1.9,9.486a4.238,4.238,0,0,1-.628,1.809,2.093,2.093,0,0,1-1.5.688l-.14.016c-3.112.225-7.829.256-7.874.256-.243,0-5.913-.092-7.633-.251-.095-.016-.2-.028-.33-.042a2.371,2.371,0,0,1-1.61-.655,3.818,3.818,0,0,1-.634-1.811c-.007-.078-.169-1.938-.169-3.761v-1.8c0-1.81.162-3.677.169-3.752a4.129,4.129,0,0,1,.627-1.806,2.2,2.2,0,0,1,1.538-.72l.1-.012c3.16-.227,7.861-.257,7.907-.257h0c.077,0,4.73.029,7.864.255l.143.017a2.263,2.263,0,0,1,1.53.674,3.877,3.877,0,0,1,.635,1.842c0,.018.169,1.921.169,3.761V381C565.344,382.853,565.177,384.736,565.175,384.75Z" transform="translate(-542.751 -370.75)"/></svg></a></li>
                        <li><a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24.004" height="19.973" viewBox="0 0 24.004 19.973"> <path id="Path_728" data-name="Path 728" d="M496.932,387.973h0A11.391,11.391,0,0,0,507.7,380.75a14.012,14.012,0,0,0,.963-5.234c0-.1-.006-.21-.016-.318l-.077-.869.584-.647c.121-.134.235-.272.344-.415l-.014.006-1.127.413-.895-.8a3.481,3.481,0,0,0-5.8,2.618l.015,2.17-2.164-.163a11.607,11.607,0,0,1-7.447-3.456,9.388,9.388,0,0,0,.105,5.085,7.706,7.706,0,0,0,3.455,4.2L498.8,385.3l-3.391,1.562a12.639,12.639,0,0,1-1.817.672,13.161,13.161,0,0,0,3.336.441m0,2a15.956,15.956,0,0,1-7.756-2.088,1,1,0,0,1,.48-1.875h.046c.142.006.283.008.425.008a10.644,10.644,0,0,0,4.449-.975,9.644,9.644,0,0,1-4.308-5.285,11.665,11.665,0,0,1-.015-6.7,10.652,10.652,0,0,1,.495-1.456,1,1,0,0,1,1.732-.168,9.66,9.66,0,0,0,7.181,4.081,5.48,5.48,0,0,1,9.133-4.122,9.9,9.9,0,0,0,2.291-1.2.981.981,0,0,1,.573-.187,1,1,0,0,1,.975,1.24,8.709,8.709,0,0,1-1.995,3.774c.015.162.023.325.024.49C510.662,384.34,504.317,389.973,496.932,389.973Z" transform="translate(-488.66 -370)"/> </svg></a></li>
                        <li><a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="13.001" height="22" viewBox="0 0 13.001 22"> <path id="Path_727" data-name="Path 727" d="M446.021,389v-8h3.219l.5-2h-3.719v-6h4v-2h-2a4.005,4.005,0,0,0-4,4v4h-3v2h3v8h2m1,2h-4a1,1,0,0,1-1-1v-7h-2a1,1,0,0,1-1-1v-4a1,1,0,0,1,1-1h2v-2a6,6,0,0,1,6-6h3a1,1,0,0,1,1,1v4a1,1,0,0,1-1,1h-3v2h3a1,1,0,0,1,.97,1.242l-1,4a1,1,0,0,1-.97.757h-2v7A1,1,0,0,1,447.021,391Z" transform="translate(-439.021 -369)" /> </svg></a></li>
                        <li><a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22"> <path id="Combined_Shape-2" data-name="Combined Shape-2" d="M398,391H388a6.007,6.007,0,0,1-6-6V375a6.007,6.007,0,0,1,6-6h10a6.007,6.007,0,0,1,6,6v10A6.006,6.006,0,0,1,398,391Zm-10-20a4.005,4.005,0,0,0-4,4v10a4,4,0,0,0,4,4h10a4,4,0,0,0,4-4V375a4,4,0,0,0-4-4Zm5.044,13.957a5,5,0,1,1,2.251-.534,5,5,0,0,1-2.251.534Zm0-8a3,3,0,1,0,3,3,3,3,0,0,0-2.561-2.968,3.09,3.09,0,0,0-.441-.032Z" transform="translate(-382 -369)"/> </svg></a></li>
                    </ul>
                </div>
                <p class="footer-text wow fadeInDown">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك </p>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <div class="footer-links wow fadeInDown">
                            <h4>قائمة أولى</h4>
                            <ul>
                                <li><a href="#">هواتف ذكية</a></li>
                                <li><a href="#">أجهزة حاسوب</a></li>
                                <li><a href="#">اكسسورات حاسوب</a></li>
                                <li><a href="#">شاشات ذكية</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4">
                        <div class="footer-links wow fadeInDown">
                            <h4>قائمة أولى</h4>
                            <ul>
                                <li><a href="#">هواتف ذكية</a></li>
                                <li><a href="#">أجهزة حاسوب</a></li>
                                <li><a href="#">اكسسورات حاسوب</a></li>
                                <li><a href="#">شاشات ذكية</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-sm-4">
                        <div class="footer-links wow fadeInDown">
                            <h4>قائمة أولى</h4>
                            <ul>
                                <li><a href="#">هواتف ذكية</a></li>
                                <li><a href="#">أجهزة حاسوب</a></li>
                                <li><a href="#">اكسسورات حاسوب</a></li>
                                <li><a href="#">شاشات ذكية</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyrights wow fadeInDown">جميع الحقوق محفوظة لعصارة © 2020</div>
    </div>
</footer>

<script type="text/javascript" src="{{ asset('user/js/jquery-3.4.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('user/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('user/js/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('user/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('user/js/jquery.form.js') }}"></script>
<script type="text/javascript" src="{{ asset('user/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('user/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('user/js/intlTelInput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('user/js/nouislider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('user/js/script.js') }}"></script>
@yield('scripts')


</body>
</html>
