@extends('user.index.parent')




@section('content')



<section class="inner-blogs wow fadeIn">
    <div class="blogs-section-title text-center">
        <h2 class="wow fadeInDown">مدونة عصارة</h2>
        <p class="wow fadeInDown">هنا تكمن عصارة معرفتنا ومعرفة مدربينا</p>
    </div>
    <div class="big-items-wrap container">
        <div class="big-items row">
            @foreach ($articlesSpecial as $article )
            <div class="col-12 {{ $loop->first?"col-lg-8":"col-lg-4" }}">
                <div class="big-blog-item wow fadeInDown">
                    <img src="{{ asset($article->image->image_url   ) }}" class="blog-img"/>
                    <a href="{{ route('home-article',[$article->id]) }}" class="blog-item-link">
                        <div class="blog-content">
                            <object>
                                <a href="{{ route('home-category',[$article->category->id]) }}" class="category">{{ $article->category->getName() }}</a>
                            </object>
                            <h3>{{ $article->title }}</h3>
                            <object>
                                <a href="{{ route('home-author',[$article->author->id]) }}" class="author">
                                    <img src="{{ asset('user/images/user.png') }}" />
                                    <span>{{ $article->author->name }}</span>
                                </a>
                            </object>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach


        </div>
    </div>
    <div class="other-blogs-wrap">
        <div class="container">
            <div class="row blog-items-row">
                @foreach ($articles as $article )
                <div class="col-6 col-sm-6 col-md-4 col-lg-3">
                    <div class="blog-item wow fadeInDown">
                        <div class="blog-img">
                            <a href="{{ route('home-article',[$article->id]) }}">
                                <img src="{{ asset($article->image->image_url) }}" />
                            </a>
                        </div>
                        <div class="blog-content">
                            <a href="#" class="author-img">
                                <img src="{{ asset('user/images/user.png') }}" />
                            </a>
                            <div class="blog-meta">
                                <a href="{{ route('home-author',[$article->author->id]) }}" class="author">{{ $article->author->name }}</a>
                                <a href="{{ route('home-category',[$article->category->id]) }}" class="category">{{ $article->category->name }}</a>
                            </div>
                            <h3><a href="#">{{ $article->title }}</a></h3>
                        </div>
                    </div>
                </div>
                @endforeach


            </div>
            {{ $articles->links('pagination.news') }}
        </div>
    </div>
</section>
@endsection







