
@extends('user.index.parent')



@section('styles')
<link rel="stylesheet" href="{{ asset('cms/plugins/toastr/toastr.min.css') }}">

@endsection

@section('content')


<section class="single-blog-section wow fadeIn">
    <div class="container">
        <div class="single-blog-wrap">
            <h1 class="title wow fadeInDown">{{ $article->title }}</h1>
            <div class="meta wow fadeInDown">
                <span class="date">{{ $article->created_at->format('d Y M') }}</span>
                <a href="#" class="cat">تسويق الكتروني</a>
            </div>
            <img class="main-img wow fadeIn" src="{{ asset($article->image->image_url ) }}">
            <div class="single-blog-content">
                <div class="blog-meta wow fadeInDown">
                    <a href="{{ route('home-author',$article->author->id) }}" class="author">
                        <img src="{{ asset('user/images/user.png') }}" />
                        <span>{{ $article->author->name }}</span>
                    </a>
                    <div class="blog-share">
                        <span>مشاركة</span>
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="17.422" height="14.496" viewBox="0 0 17.422 14.496">
                                <path id="Path_920" data-name="Path 920" d="M494.664,383.045h0a8.267,8.267,0,0,0,7.815-5.242,10.17,10.17,0,0,0,.7-3.8c0-.075,0-.152-.012-.231l-.056-.631.424-.47c.088-.1.171-.2.25-.3l-.01,0-.818.3-.65-.581a2.527,2.527,0,0,0-4.21,1.9l.011,1.575-1.571-.118a8.425,8.425,0,0,1-5.405-2.508,6.814,6.814,0,0,0,.076,3.691,5.593,5.593,0,0,0,2.508,3.049l2.307,1.42-2.461,1.134a9.174,9.174,0,0,1-1.319.488,9.553,9.553,0,0,0,2.421.32m0,1.452a11.581,11.581,0,0,1-5.629-1.515.726.726,0,0,1,.348-1.361h.033c.1,0,.205.006.308.006a7.725,7.725,0,0,0,3.229-.708,7,7,0,0,1-3.127-3.836,8.466,8.466,0,0,1-.011-4.862,7.735,7.735,0,0,1,.359-1.057.725.725,0,0,1,1.257-.122A7.011,7.011,0,0,0,496.645,374a3.978,3.978,0,0,1,6.629-2.992,7.184,7.184,0,0,0,1.663-.872.712.712,0,0,1,.416-.136.728.728,0,0,1,.708.9,6.321,6.321,0,0,1-1.448,2.739c.011.118.017.236.017.356C504.629,380.408,500.024,384.5,494.664,384.5Z" transform="translate(-488.66 -370)" />
                            </svg>
                        </a>
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.436" height="15.968" viewBox="0 0 9.436 15.968">
                                <path id="Path_919" data-name="Path 919" d="M444.1,383.516V377.71h2.336l.363-1.452h-2.7V371.9H447v-1.452h-1.452a2.907,2.907,0,0,0-2.9,2.9v2.9h-2.177v1.452h2.177v5.806H444.1m.726,1.452h-2.9a.726.726,0,0,1-.726-.726v-5.081h-1.452a.726.726,0,0,1-.726-.726v-2.9a.726.726,0,0,1,.726-.726H441.2v-1.452A4.355,4.355,0,0,1,445.553,369h2.177a.725.725,0,0,1,.726.726v2.9a.726.726,0,0,1-.726.726h-2.177v1.452h2.177a.725.725,0,0,1,.7.9l-.726,2.9a.725.725,0,0,1-.7.549h-1.452v5.081A.726.726,0,0,1,444.827,384.968Z" transform="translate(-439.021 -369)" />
                            </svg>
                        </a>
                        <a href="#">
                            <svg id="Component_64_1" data-name="Component 64 – 1" xmlns="http://www.w3.org/2000/svg" width="15.66" height="16.333" viewBox="0 0 15.66 16.333">
                                <path id="Path_784" data-name="Path 784" d="M137.407,1171.058c.052-2.642.1-5.138-1.211-6.476a3.987,3.987,0,0,0-3-1.029h-.05a3.986,3.986,0,0,0-2.207.672c-.133-.408-.559-.447-.964-.447-.171,0-.368.008-.593.018-.3.013-.639.027-1.031.027-.3,0-.579-.008-.864-.025h0a.647.647,0,0,0-.655.645c.077,3.6.025,5.9-.009,7.424-.041,1.79-.054,2.393.211,2.663a.587.587,0,0,0,.444.154h2.977a.591.591,0,0,0,.449-.165c.254-.262.236-.791.2-1.846-.027-.8-.065-1.9,0-3.38,0-1.688.495-1.822,1.14-1.822.717,0,.9.39.9,1.9.061,1.457.024,2.535,0,3.322-.035,1.046-.053,1.57.2,1.832a.591.591,0,0,0,.448.164h2.977a.647.647,0,0,0,.646-.646C137.368,1173.039,137.388,1172.031,137.407,1171.058Zm-3.681-4.356a1.975,1.975,0,0,0-1.485-.524,2.218,2.218,0,0,0-1.676.6c-.882.9-.839,2.7-.785,4.972.013.524.025,1.073.028,1.641h-1.684v-8.3h1.566v.663a.678.678,0,0,0,.673.641l.047,0a.672.672,0,0,0,.535-.331,2.508,2.508,0,0,1,2.142-1.214l.1,0c2.165,0,2.93,1.026,2.93,3.919v4.626h-1.684c0-.615.011-1.2.021-1.737C134.5,1169.162,134.526,1167.516,133.726,1166.7Z" transform="translate(-121.767 -1158.352)" />
                                <path id="Path_785" data-name="Path 785" d="M123.833,1164.558c0-.667-.65-.667-1.038-.667-.183,0-.395.009-.633.018-.3.013-.655.027-1.049.027-.31,0-.6-.009-.9-.027h0a.593.593,0,0,0-.45.153c-.265.27-.251.873-.211,2.662.034,1.524.085,3.827.009,7.423a.648.648,0,0,0,.646.649h2.98a.587.587,0,0,0,.443-.153c.264-.27.251-.872.211-2.658C123.808,1170.46,123.756,1168.158,123.833,1164.558Zm-2.98,8.949V1165.2h1.689v8.307Z" transform="translate(-119.343 -1158.464)" />
                                <path id="Path_786" data-name="Path 786" d="M121.6,1155.75a2.269,2.269,0,0,0-1.765.747,2.452,2.452,0,0,0-.587,1.628,2.353,2.353,0,1,0,4.706,0A2.266,2.266,0,0,0,121.6,1155.75Zm0,3.466a1.089,1.089,0,1,1,1.059-1.063A1.084,1.084,0,0,1,121.6,1159.216Z" transform="translate(-119.249 -1155.75)" />
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="blog-text-content wow fadeIn">
                {!! $article->full_description !!}

            </div>

        </div>
        </div>

        @foreach ($article->comments as $comment)
 <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{ asset('user/images/user.png') }}" alt="User Image">

            <div class="comment-text">
              <span class="username">
                {{ $comment->user->name }}
                <br>
                <span class="text-muted float-right">{{ $comment->created_at->diffForHumans() }}</span>
                <br>
              </span><!-- /.username -->
           {!! $comment->comment !!}
            </div>

            <!-- /.comment-text -->
          </div>

        @endforeach
        @auth
        <div class="card-footer">
            <form id="create_comment" form="create_comment"action="{{ route('comments-user',$article->id) }}" method="post">

              <img class="img-fluid img-circle img-sm" src="{{ asset('cms/dist/img/user4-128x128.jpg') }}" alt="Alt Text">

                <input type="text" name="comment" id="comment"class="form-control form-control-sm" placeholder="Add Comment">

                <button type="submit" class="btn btn-primary">Save</button>


            </form>
          </div>
        @endauth



    </div>
</section>
<section class="related-blog-section wow fadeIn">
    <div class="container">
        <h2 class="related-title wow fadeInDown">مواضيع ذات صلة</h2>
        <div class="row related-row">
            @foreach ( $articles as $art)
            <div class="col-6 col-sm-6 col-md-3">
                <div class="blog-item wow fadeInDown">
                    <div class="blog-img">
                        <a href="{{ route('home-article',[$art->id]) }}">
                            <img src="{{asset($art->image->image_url)  }}" />
                        </a>
                    </div>
                    <div class="blog-content">
                        <a href="#" class="author-img">
                            <img src="{{ asset('user/images/user.png') }}" />
                        </a>
                        <div class="blog-meta">
                            <a href="{{ route('home-author',[$art->author->id]) }}" class="author">{{ $art->author->name }}</a>
                            <a href="{{ route('home-category',[$art->category->id]) }}" class="category">{{ $art->category->name }}</a>
                        </div>
                        <h3><a href="{{ route('home-article',[$art->id]) }}">{{ $art->title }}</a></h3>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</section>
{{-- @endforeach --}}


@endsection

@section('scripts')
 <!-- bs-custom-file-input -->
<script src="{{ asset('cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script src="{{ asset('Js/axios.js') }}"> </script>

<!-- Toastr -->
<script src="{{ asset('cms/plugins/toastr/toastr.min.js') }}"></script>


    <script>
        //  $('#create_form').validate();
        $('#create_comment').submit(function(e){
            e.preventDefault();

            var data = new FormData(this);

            axios.post('{{ route('comments-user',$article->id) }}',data)
            .then(function (response) {
                console.log(response);
                document.getElementById('create_comment').reset();
                showToaster(response.data.message, true);
                window.location.reload();
            })
            .catch(function (error) {
                console.log(error.response);
                showToaster(error.response.data.message, false);
            });
        });

        function showToaster(message, status){
            if(status){
                toastr.success(message);
            }else{
                toastr.error(message);
            }
        }
    </script>

@endsection




