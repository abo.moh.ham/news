@if ($paginator->hasPages())
<div class="text-center custom-pagination white wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">
    @if ($paginator->onFirstPage())

    <a  class="first-page disabled">
        <svg xmlns="http://www.w3.org/2000/svg" width="8" height="14" viewBox="0 0 8 14">
            <path id="Shape" d="M5.586,7,.293,12.293a1,1,0,0,0,1.414,1.414l6-6a1,1,0,0,0,0-1.414l-6-6A1,1,0,0,0,.293,1.707Z"></path>
        </svg>
    </a>
@else
<a href="{{ $paginator->previousPageUrl() }}" class="first-page">
    <svg xmlns="http://www.w3.org/2000/svg" width="8" height="14" viewBox="0 0 8 14">
        <path id="Shape" d="M5.586,7,.293,12.293a1,1,0,0,0,1.414,1.414l6-6a1,1,0,0,0,0-1.414l-6-6A1,1,0,0,0,.293,1.707Z"></path>
    </svg>
</a>
@endif
    <ul class="pagination-ul">
        @foreach ($elements as $element)
        @if (is_string($element))

        <li><a class="active" >{{ $element }}</a></li>

        @endif
        @if (is_array($element))
        @foreach ($element as $page => $url)
            @if ($page == $paginator->currentPage())
            <li class=" active" aria-current="page" ><a >{{  $page  }}</a></li>
            @else
                <li ><a href="{{ $url }}">{{ $page }}</a></li>
            @endif
        @endforeach
    @endif
        @endforeach
    </ul>
    @if ($paginator->hasMorePages())
        <a class="last-page"  href="{{ $paginator->nextPageUrl() }}" rel="next">
            <svg xmlns="http://www.w3.org/2000/svg" width="8" height="14" viewBox="0 0 8 14">
                <path id="Shape" d="M5.586,7,.293,12.293a1,1,0,0,0,1.414,1.414l6-6a1,1,0,0,0,0-1.414l-6-6A1,1,0,0,0,.293,1.707Z"></path>
            </svg>
        </a>
@else
<a class="last-page"   rel="next">
    <svg xmlns="http://www.w3.org/2000/svg" width="8" height="14" viewBox="0 0 8 14">
        <path id="Shape" d="M5.586,7,.293,12.293a1,1,0,0,0,1.414,1.414l6-6a1,1,0,0,0,0-1.414l-6-6A1,1,0,0,0,.293,1.707Z"></path>
    </svg>
</a>
@endif

</div>
@endif
