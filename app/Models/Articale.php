<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articale extends Model
{
    use HasFactory;
    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function image(){
        return $this->belongsTo(Image::class,'image_id','id');
    }

    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }
    public function author(){
        return $this->belongsTo(Author::class,'author_id','id');
    }

}
