<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class Author extends Authenticatable
{
    use HasFactory;

    use HasFactory;
    public function categories(){
        return $this->belongsToMany(Category::class,'author_categories');
    }
    public function articles(){
        return $this->hasMany(Articale::class,'author_id');
    }
}
