<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthorAuthController extends Controller
{
    //
    public function showLogin()
    {
        return response()->view('auth2.login');
    }



    public function login(Request $request)
    {
        $validator = Validator($request->all(), [
            'email' => 'required|email|exists:authors,email',
            'password' => 'required|string|min:3',
            // 'remember_me' => 'boolean',
        ]);
        // $credentials = $request->only('email', 'password');

        // if (Auth::attempt($credentials)) {
        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];

        if (!$validator->fails()) {
            // if (Auth::guard('author')->attempt($credentials)) {
                if (Auth::guard('author')->attempt($credentials)) {

                return response()->json(['message' => 'Logged in succesfully'], 200);
                // return redirect()->route('cms.dashboard');
            } else {
                return response()->json(['message' => 'Login failed, please login credentials'], 400);
            }
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], 400);
        }
    }

    public function editPassword(Request $request)
    {
        return response()->view('auth2.edit-password');
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator($request->all(), [
            'current_password' => 'required|string|password:admin',
            'new_password' => 'required|string|confirmed',
            'new_password_confirmation' => 'required|string',
            // 'new_password_confirmation' => 'required|string|same:new_password',
        ]);

        if (!$validator->fails()) {
            // $user = User::findOrFail($request->user('admin')->id);
            $user = $request->user('author');
            $user->password = Hash::make($request->get("new_password"));
            $isSaved = $user->save();
            return response()->json(['message' => 'Password changed successfully'], 200);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], 400);
        }
    }

    public function logout(Request $request)
    {
        auth('author')->logout();
        // Auth::guard('guardName')->user()
        // $request->user()->logout();
        return redirect()->route('auth-author.login.view');
    }
}
