<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class settingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting=Setting::first();
        return response()->view('cms.settings.edit',['setting'=>$setting]);
        //
        // $settings=Setting::all();
        // return response()->view('cms.settings.index',['settings'=>$settings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('cms.settings.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validator = Validator($request->all(), [
            'email' => 'required|email',
            'mobile' => 'required|numeric|min:2',
            'phone' => 'required|numeric|min:2',
            'working_time' => 'required|string|min:2|max:45',
            'box_office' => 'required|string|min:2|max:45',
            'adress' => 'required|string|min:2|max:45',

        ]);

         if (!$validator->fails()) {
            $setting=Setting::first();
            if(!$setting)
                $setting = new Setting();
            $setting->email = $request->get('email');
            $setting->mobile = $request->get('mobile');
            $setting->phone = $request->get('phone');
            $setting->working_time = $request->get('working_time');
            $setting->box_office = $request->get('box_office');
            $setting->adress = $request->get('adress');
            $isSaved = $setting->save();
         return response()->json(['message' => $isSaved ? 'setting created successfully' : 'Failed to create setting!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }
}
