<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories=Category::all();
        return response()->view('cms.category.index',['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('cms.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:2|max:45',
            'description' => 'required|string|min:2|max:45',


        ]);

         if (!$validator->fails()) {
            $category = new Category();
            $category->name = $request->get('name');
            $category->description = $request->get('description');

            $category->status = $request->has('status');
            $isSaved = $category->save();
         return response()->json(['message' => $isSaved ? 'Categories created successfully' : 'Failed to create Category!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category=Category::FindOrFail($id);

        return response()->view('cms.category.edit',['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:2|max:45',
            'description' => 'required|string|min:2|max:45',


        ]);

         if (!$validator->fails()) {
            $category = Category::findOrFail($id);
            $category->name = $request->get('name');
            $category->description = $request->get('description');

            $category->status = $request->has('status');
            $isSaved = $category->save();
         return response()->json(['message' => $isSaved ? 'Category Updated successfully' : 'Failed to Update Category!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $IsDeleted=Category::destroy($id);
        if($IsDeleted){

            return response()->json(['titel'=>'Deleted','message'=>'Category Deleted Successfully','icon'=>'success'],200);

        }else{
            return response()->json(['titel'=>'Failed','message'=>' Deleted Category Failed','icon'=>'error'],400);


        }
    }
}
