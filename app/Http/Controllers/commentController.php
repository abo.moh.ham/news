<?php

namespace App\Http\Controllers;

use App\Models\Articale;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;

class commentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $comments=Comment::with('user','article')->get();
        return response()->view('cms.comment.index',['comments'=>$comments]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // $users=User::where('status','=',1)->get();
        $articles=Articale::all();
        return response()->view('cms.comment.create',compact('articles'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'comment' => 'required|string|min:2|max:45',
            'article_id'=>'required|exists:articales,id',
            // 'user_id'=>'required|exists:users,id',
        ]);

         if (!$validator->fails()) {
            $comment = new Comment();
            $comment->comment = $request->get('comment');
            $comment->articale_id = $request->get('article_id');
            // $comment->user_id = $request->get('user_id');
            $comment->user_id=User::orderByRaw('rand()')->first()->id;
                // $comment = Comment::with('user')->find(1);


            $isSaved = $comment->save();
         return response()->json(['message' => $isSaved ? 'Comments created successfully' : 'Failed to create comment!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $comment=Comment::FindOrFail($id);

        return response()->view('cms.comment.edit',['comment'=>$comment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator($request->all(), [
            'comment' => 'required|string|min:2|max:45',


        ]);

         if (!$validator->fails()) {
            $comment = Comment::findOrFail($id);
            $comment->comment = $request->get('comment');

            $isSaved = $comment->save();
         return response()->json(['message' => $isSaved ? 'comment Updated successfully' : 'Failed to Update comment!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $isDeleted = Comment::destroy($id);
        if ($isDeleted) {
            // return redirect()->back();
            return response()->json(['title' => 'Deleted!', 'message' => 'Comment Deleted Successfully', 'icon' => 'success'], 200);
        } else {
            return response()->json(['title' => 'Failed!', 'message' => 'Delete Comeent failed', 'icon' => 'error'], 400);
        }


    }
}
