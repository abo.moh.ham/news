<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class authorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories=Category::all();
        $authors=Author::with('categories');

        if($request->search){
            $authors->where(function($q) use($request){
                $q->where('name','like',"%{$request->search}%")
                ->orWhere('email','like',"%{$request->search}%")
                ->orWhere('email','like',"%{$request->search}%");
            });

        }

        if($request->searchCtegory){
            $authors->whereHas('categories',function($q) use($request){
                $q->where('category_id',$request->searchCategory);
            });
        }

        return response()->view('cms.authors.index',compact('categories','authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories=Category::where('status','=',1)->get();

        return response()->view('cms.authors.create',compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:2|max:45',
            'email' => 'required|email|unique:authors,email',
            'mobile' => 'required|numeric|min:2|unique:authors,mobile',
            'password' => 'required|min:6',
            'gender' => 'required|in:M,F|string',
            'cateogry_id'=>'required|array|min:1'

        ]);
         if (!$validator->fails()) {
            $author = new Author();
            $author->name = $request->get('name');
            $author->email = $request->get('email');
            $author->mobile = $request->get('mobile');
            $author->password = Hash::make($request->get('password'));
            $author->status = $request->has('status');
            $author->gender = $request->get('gender');
            $isSaved = $author->save();
            $author->categories()->attach($request->cateogry_id);
         return response()->json(['message' => $isSaved ? 'Author created successfully' : 'Failed to create Author!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $author=Author::findOrFail($id);
        $categories=Category::where('status','=',1)->get();

        return response()->view('cms.authors.edit',['author'=>$author,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:2|max:45',
            'email' => 'required|email|unique:authors,email,'.$id,
            'mobile' => 'required|numeric|min:2|unique:authors,mobile,'.$id,
            // 'password' => 'nullable|min:6',
            'gender' => 'required|in:M,F|string',
            'category_id'=>'required|array|min:1'
            // 'category_id'  =>  'required|exists:categories,id',

        ]);

         if (!$validator->fails()) {
            $author = Author::with('categories')->findOrFail($id);
            $author->name = $request->get('name');
            $author->email = $request->get('email');
            $author->mobile = $request->get('mobile');
            // $author->password = Hash::make($request->get('password'));
            $author->status = $request->has('status');
            $author->gender = $request->get('gender');
            // $author->cateogry_id = $request->get('cateogry_id');


            $isSaved = $author->save();

            $author->categories()->sync($request->category_id);

         return response()->json(['message' => $isSaved ? 'Author Updated successfully' : 'Failed to Update Author!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $IsDeleted=Author::destroy($id);
        if($IsDeleted){

            return response()->json(['titel'=>'Deleted','message'=>'Author Deleted Successfully','icon'=>'success'],200);

        }else{
            return response()->json(['titel'=>'Failed','message'=>' Deleted Author Failed','icon'=>'error'],400);


        }
    }
    }

