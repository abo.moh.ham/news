<?php

namespace App\Http\Controllers;

use App\Models\Articale;
use App\Models\Author;
use App\Models\Category;
use App\Models\Comment;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articlesSpecial = Articale::where('special', 1)->latest()->with(['image', 'category', 'author'])->limit(5)->get();

        $articles = Articale::whereNotIn('id', $articlesSpecial->pluck('id')->toArray())->latest()->with(['image', 'category', 'author'])->paginate(12);



        return view('user.index.blog', compact('articles', 'articlesSpecial'));
    }
    public function show($id)
    {

        // $article=Articale::where('id',$id)->first();
        $article = Articale::with('image', 'category', 'author', 'comments.user')->FindOrFail($id);
        $articles = Articale::where('id', '!=', $article->id)->where('category_id', $article->category_id)
            ->latest()->with(['image', 'category', 'author'])->latest()->limit(4)->get();

        return view('user.index.blog-single', compact('article', 'articles'));
    }



    public function StorCommment($id, Request $request)
    {

        $validator = Validator($request->all(), [
            'comment' => 'string|min:2|max:45',
            // 'user_id'=>'required|exists:users,id',
        ]);

        if (!$validator->fails()) {
            $comment = new Comment();
            $comment->comment = $request->get('comment');
            // $comment->user_id = $request->get('user_id');
            $comment->user_id = auth('user')->user()->id;
            $comment->articale_id = $id;


            $isSaved = $comment->save();
            return response()->json(['message' => $isSaved ? 'Comments created successfully' : 'Failed to create comment!'], $isSaved ? 201 : 400);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], 422);
        }
    }


    public function showAuthor($id)
    {

        $author = Author::with('categories')->FindOrFail($id);

        //$articales = Articale::where('author_id', $author->id)
        $articales=$author->articles()
        ->with('image', 'author', 'category')->paginate(8);
        return view('user.index.author',
            compact('author', 'articales')
        );
    }


    public function showCategory($id)
    {

        $category = Category::with('authors')->FindOrFail($id);

        //$articales = Articale::where('author_id', $author->id)
        $articales=$category->articles()
        ->with('image', 'author', 'category')->paginate(8);
        return view('user.index.category',
            compact('category', 'articales')
        );
    }
}
