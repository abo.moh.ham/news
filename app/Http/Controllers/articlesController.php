<?php

namespace App\Http\Controllers;

use App\Helpers\FileUpload;
use App\Models\Articale;
use App\Models\Author;
use App\Models\Category;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class articlesController extends Controller
{
    use FileUpload;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        // if($request->search){
        //         $articles=Articale::with('image','author','category')
        //         ->where('author_id',auth('author')->id())
        //         ->where('title','like','%{$request->search}%')
        //         ->orWhere('shrt_description','like','%{$request->search}%')
        //         ->get();


        // }
        if( $request->search){
            $articles=Articale::with('image','author','category')
            ->where(function($q) use($request){
                $q->where('title','like',"%{$request->search}%")
                ->orWhere('shrt_description','like',"%{$request->search}%");
            })
                ->get();

        }
        else{
            if(auth('author')->check()){
                $articles=Articale::with('image','author','category')->where('author_id',auth('author')->id())->get();
            }else
            $articles=Articale::with('image','author','category')->get();

        }


        return response()->view('cms.articale.index',['articles'=>$articles]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories=Category::where('status','=',1)->get();
        $authors=Author::where('status','=',1)->get();

        return response()->view('cms.articale.create',compact('categories','authors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'title' => 'required|string|min:2|max:45',
            'shrt_description' => 'required|string|min:2|max:45',
            'full_description' => 'required|string|min:2|max:3000',
            'category_id'  =>  'required|exists:categories,id',
            'author_id'  =>  'required|exists:authors,id',
            'image' => 'required|image|mimes:png,jpg,jpeg|max:10048',

        ]);

        if (!$validator->fails()) {
            $article = new Articale();

            $article->title = $request->get('title');
            $article->shrt_description = $request->get('shrt_description');
            $article->full_description = $request->get('full_description');
            // $article->active = $request->has('active') ? true : false;
            $article->category_id=$request->get('category_id');
            $article->author_id=$request->get('author_id');
            $article->special=$request->has('special')?1:0;

            $this->uploadFile($request->file('image'), 'images/articles/', 'public', 'abc_article_' . time());
            $image = new Image();
            $image->image_url=Storage::url($this->filePath);
            $image->status='Active';
            $image->name=$request->file('image')->getClientOriginalName();
            $image->size=$request->file('image')->getSize();
            $image->save();
            $article->image_id=$image->id;

            $isSaved = $article->save();



            // if ($request->hasFile('image')) {
            //     $this->uploadFile($request->file('image'), 'images/articles/', 'public', 'abc_article_' . time());
            //     // $article->image = Storage::url($this->filePath);
            //     $article->image()->create([
            //         'image_url'=>Storage::url($this->filePath),
            //         'status'=>'Active',
            //         'name'=>$request->file('image')->getClientOriginalName(),
            //         'size'=>$request->file('image')->getSize()
            //     ]);
            //     // $image = new Image();
            //     // $image->image_url=Storage::url($this->filePath);
            //     // $image->status='Active';
            //     // $image->name=$request->file('image')->getClientOriginalName();
            //     // $image->size=$request->file('image')->getSize();
            //     // $image->save();
            //     // $article->image_id=$image->id;
            // }



            return response()->json(['message' => $isSaved ? 'Article created successfully' : 'Failed to create Article!'], $isSaved ? 201 : 400);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(auth('author')->check()){
            $article=Articale::where('author_id',auth('author')->id())->findOrFail($id);

        }else
        $article=Articale::findOrFail($id);
        $categories=Category::where('status','=',1)->get();
        $authors=Author::where('status','=',1)->get();
        return response()->view('cms.articale.edit', compact('categories','authors','article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator($request->all(), [
            'title' => 'required|string|min:2|max:45',
            'shrt_description' => 'required|string|min:2|max:2222',
            'full_description' => 'required|string|min:2|max:22222',
            'category_id'  =>  'required|exists:categories,id',
            'author_id'  =>  'required|exists:authors,id',

            'image' => 'nullable|image|mimes:png,jpg,jpeg|max:10048',
        ]);

        if (!$validator->fails()) {
            $article = Articale::findOrFail($id);

            if ($request->hasFile('image')) {
                $oldImage=$article->image;
                $this->uploadFile($request->file('image'), 'images/articles/', 'public', 'abc_article_' . time());
                $image = new Image();
                $image->image_url=Storage::url($this->filePath);
                $image->status='Active';
                $image->name=$request->file('image')->getClientOriginalName();
                $image->size=$request->file('image')->getSize();
                $image->save();
                $article->image_id=$image->id;
            }
            $article->title = $request->get('title');
            $article->shrt_description = $request->get('shrt_description');
            $article->full_description = $request->get('full_description');
            $article->category_id  =$request->get('category_id');
            $article->author_id=$request->get('author_id');
            $article->special=$request->has('special');

            $isSaved = $article->save();
            if ($request->hasFile('image'))
                $oldImage->delete();
            return response()->json(['message' => $isSaved ? 'article created successfully' : 'Failed to create article!'], $isSaved ? 201 : 400);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
            //
            $isDeleted = Articale::destroy($id);
            if ($isDeleted) {
                // return redirect()->back();
                return response()->json(['title' => 'Deleted!', 'message' => 'Article Deleted Successfully', 'icon' => 'success'], 200);
            } else {
                return response()->json(['title' => 'Failed!', 'message' => 'Delete Article failed', 'icon' => 'error'], 400);
            }
    }
}
