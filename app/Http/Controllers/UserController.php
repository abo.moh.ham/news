<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users=User::all();
        return response()->view('cms.user.index',['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('cms.user.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:2|max:45',
            'email' => 'required|email|unique:users,email',
            'mobile_number' => 'required|numeric|min:2|unique:users,mobile_number',
            'password' => 'required|string:users,password',
            'gender' => 'required|in:M,F|string',
            'status' => 'required|string|min:2|max:45'

        ]);

         if (!$validator->fails()) {
            $user = new User();
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->mobile_number = $request->get('mobile_number');
            $user->password = Hash::make($request->get('password'));
            $user->status = $request->get('status');
            $user->gender = $request->get('gender');
            $isSaved = $user->save();
         return response()->json(['message' => $isSaved ? 'Users created successfully' : 'Failed to create User!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user=User::FindOrFail($id);

        return response()->view('cms.user.edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:2|max:45',
            'email' => 'required|email|unique:users,email,'.$id,
            'mobile_number' => 'required|numeric|min:2|unique:users,mobile_number,'.$id,
            'gender' => 'required|in:M,F|string',
            'status' => 'required|string|min:2|max:45'

        ]);

         if (!$validator->fails()) {
            $user = user::findOrFail($id);
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->mobile_number = $request->get('mobile_number');
            $user->status = $request->get('status');
            $user->gender = $request->get('gender');
            $isSaved = $user->save();
         return response()->json(['message' => $isSaved ? 'user Updated successfully' : 'Failed to Update user!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $IsDeleted=User::destroy($id);
        if($IsDeleted){

            return response()->json(['titel'=>'Deleted','message'=>'User Deleted Successfully','icon'=>'success'],200);

        }else{
            return response()->json(['titel'=>'Failed','message'=>' Deleted User Failed','icon'=>'error'],400);


        }
    }
}
