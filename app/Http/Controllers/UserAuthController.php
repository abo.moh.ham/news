<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserAuthController extends Controller
{
    //

    public function showLogin()
    {
        return response()->view('auth3.login');
    }

    public function login(Request $request)
    {
        $validator = Validator($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|string|min:3',
        ]);

        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];

        if (!$validator->fails()) {
            // if (Auth::guard('author')->attempt($credentials)) {
                if (Auth::guard('user')->attempt($credentials)) {

                return response()->json(['message' => 'Logged in succesfully'], 200);
                // return redirect()->route('cms.dashboard');
            } else {
                return response()->json(['message' => 'Login failed, please login credentials'], 400);
            }
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], 400);
        }
    }

    public function logout(Request $request)
    {
        auth('user')->logout();

        return redirect()->route('home');
    }
}
