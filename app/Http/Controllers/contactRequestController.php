<?php

namespace App\Http\Controllers;

use App\Models\ContactRequest;
use Illuminate\Http\Request;

class contactRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $contacts=ContactRequest::all();
        return response()->view('cms.ContactRequest.index',['contacts'=>$contacts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('cms.ContactRequest.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'title' => 'required|string|min:2|max:45',

            'message' => 'required|string|min:2|max:45',
        ]);

         if (!$validator->fails()) {
            $contact = new ContactRequest();
            $contact->title = $request->get('title');

            $contact->message = $request->get('message');
            $contact->user_id=auth('web')->user()->id;
            $isSaved = $contact->save();
         return response()->json(['message' => $isSaved ? 'Contact Request created successfully' : 'Failed to create Contact Request !'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $contact=ContactRequest::FindOrFail($id);
        return response()->view('cms.ContactRequest.edit',['contact'=>$contact]);
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator($request->all(), [
            'title' => 'required|string|min:2|max:45',

            'message' => 'required|string|min:2|max:45',
        ]);

         if (!$validator->fails()) {
            $contact = ContactRequest::findOrFail($id);
          $contact->title = $request->get('title');

            $contact->message = $request->get('message');
            $contact->user_id=auth('web')->user()->id;
            $isSaved = $contact->save();

         return response()->json(['message' => $isSaved ? 'Contact Rerquest Updated successfully' : 'Failed to Update Contact Request!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $IsDeleted=ContactRequest::destroy($id);
if($IsDeleted){
    // return redirect()->back();
    return response()->json(['titel'=>'Deleted','message'=>'Contact Request Deleted Successfully','icon'=>'success'],200);

}else{
    return response()->json(['titel'=>'Failed','message'=>' Deleted Contact Request Failed','icon'=>'error'],400);


}
    }
}
