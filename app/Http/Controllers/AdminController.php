<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request )
    {
        //
        if( $request->search){
        $admins=Admin::withCount('permissions')
        ->where(function($q) use($request){
            $q->where('name','like',"%{$request->search}%")
            ->orWhere('email','like',"%{$request->search}%")
            ->orWhere('Mobile','like',"%{$request->search}%");
        })


        ->paginate(10);

        }else{
        $admins=Admin::withCount('permissions')->paginate(10);

        }

        return response()->view('cms.admin.index',['admins'=>$admins]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('cms.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:2|max:45',
            'email' => 'required|email|unique:admins,email',
            'mobile' => 'required|numeric|min:2|unique:admins,mobile',
            'password' => 'required|min:6',
            'status' => 'required|string|min:2|max:45',
            'gender' => 'required|in:M,F|string'
        ]);

         if (!$validator->fails()) {
            $admin = new Admin();
            $admin->name = $request->get('name');
            $admin->email = $request->get('email');
            $admin->mobile = $request->get('mobile');
            $admin->password = Hash::make($request->get('password'));
            $admin->status = $request->get('status');
            $admin->gender = $request->get('gender');
            $isSaved = $admin->save();
         return response()->json(['message' => $isSaved ? 'Admin created successfully' : 'Failed to create admin!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $admin=Admin::FindOrFail($id);

        return response()->view('cms.admin.edit',['admin'=>$admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:2|max:45',
            'email' => 'required|email|unique:admins,email,'.$id,
            'mobile' => 'required|numeric|digits:10|unique:admins,mobile,'.$id,
            'status' => 'required|string|min:2|max:45',
            'gender' => 'required|in:M,F|string',
            // 'password' => 'required|min:6',

        ]);

         if (!$validator->fails()) {
            $admin = Admin::findOrFail($id);
            $admin->name = $request->get('name');
            $admin->email = $request->get('email');
            $admin->mobile = $request->get('mobile');
            $admin->status = $request->get('status');
            $admin->gender = $request->get('gender');
            // $admin->password = Hash::make($request->get('password'));

            $isSaved = $admin->save();
         return response()->json(['message' => $isSaved ? 'Admin created successfully' : 'Failed to create admin!'], $isSaved ? 201 : 400);

        } else {
           return response()->json(['message' => $validator->getMessageBag()->first()], 422);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $isDeleted = Admin::findOrFail($id)->delete();
        if ($isDeleted) {
            return response()->json(['title' => 'Deleted!', 'message' => 'Admin Deleted Successfully', 'icon' => 'success'], 200);
        } else {
            return response()->json(['title' => 'Failed!', 'message' => 'Delete admin failed', 'icon' => 'error'], 400);
        }
        // $admin=Admin::findOrFail($id);
// $IsDeleted=$admin->delete();
// if($IsDeleted){
    // return redirect()->back()->with('message','Admin Deleted Successfully');
// }

    }
}
