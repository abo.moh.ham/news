<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //NOTE: php artisan cache:clear

        // ROLES
        // Role::create(['name' => 'User', 'guard_name' => 'user']);

        //ADMIN PERMISSIONS
        Permission::create(['name' => 'create-admins', 'guard_name' => 'admin']);
        Permission::create(['name' => 'read-admins', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update-admins', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete-admins', 'guard_name' => 'admin']);

        Permission::create(['name' => 'create-users', 'guard_name' => 'admin']);
        Permission::create(['name' => 'read-users', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update-users', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete-users', 'guard_name' => 'admin']);

        Permission::create(['name' => 'create-articles', 'guard_name' => 'admin']);
        Permission::create(['name' => 'read-articles', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update-articles', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete-articles', 'guard_name' => 'admin']);


        Permission::create(['name' => 'create-roles', 'guard_name' => 'admin']);
        Permission::create(['name' => 'read-roles', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update-roles', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete-roles', 'guard_name' => 'admin']);

        Permission::create(['name' => 'create-permissions', 'guard_name' => 'admin']);
        Permission::create(['name' => 'read-permissions', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update-permissions', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete-permissions', 'guard_name' => 'admin']);



        Permission::create(['name' => 'create-contact_requests', 'guard_name' => 'admin']);
        Permission::create(['name' => 'read-contact_requests', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update-contact_requests', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete-contact_requests', 'guard_name' => 'admin']);

        Permission::create(['name' => 'create-comments', 'guard_name' => 'admin']);
        Permission::create(['name' => 'read-comments', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update-comments', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete-comments', 'guard_name' => 'admin']);


        Permission::create(['name' => 'create-settings', 'guard_name' => 'admin']);
        Permission::create(['name' => 'read-settings', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update-settings', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete-settings', 'guard_name' => 'admin']);

        Permission::create(['name' => 'create-authors', 'guard_name' => 'admin']);
        Permission::create(['name' => 'read-authors', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update-authors', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete-authors', 'guard_name' => 'admin']);




        Permission::create(['name' => 'create-categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'read-categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update-categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete-categories', 'guard_name' => 'admin']);






        // Permission::create(['name' => 'create-', 'guard_name' => 'admin']);
        // Permission::create(['name' => 'read-', 'guard_name' => 'admin']);
        // Permission::create(['name' => 'update-', 'guard_name' => 'admin']);
        // Permission::create(['name' => 'delete-', 'guard_name' => 'admin']);
        $admin = Admin::first();
        $admin->permissions()->attach(Permission::get());
    }
}
