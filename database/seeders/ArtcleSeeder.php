<?php

namespace Database\Seeders;

use App\Models\Articale;
use Illuminate\Database\Seeder;

class ArtcleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Articale::factory(100)->create();

    }
}
