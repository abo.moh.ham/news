<?php

use App\Models\Author;
use App\Models\authors;
use App\Models\categories;
use App\Models\Category;
use App\Models\Image;
use App\Models\images;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articales', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('title');
            $table->string('shrt_description');
            $table->text('full_description');
            $table->integer('seen_count')->default(0);
            $table->boolean('special')->default(0);

            $table->foreignIdFor(Author::class);
            $table->foreign('author_id')->on('authors')->references('id');



            $table->foreignIdFor(Category::class);
            $table->foreign('category_id')->on('categories')->references('id');

            // $table->string('image');
            $table->foreignIdFor(Image::class);
            $table->foreign('image_id')->on('images')->references('id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articales');
    }
}
