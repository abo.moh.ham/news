<?php

use App\Models\Articale;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('comment',45);

            $table->foreignIdFor(User::class);
            // $table->integer('user_id');
            $table->foreign('user_id')->on('users')->references('id');


            $table->foreignIdFor(Articale::class);
            $table->foreign('articale_id')->on('articales')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
