<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticaleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'title'=>$this->faker->sentence(3),
            'shrt_description'=>$this->faker->sentence(4),
            'full_description'=>$this->faker->paragraph(6),
            'special'=>$this->faker->boolean(10),
            'author_id'=>Author::orderByRaw("rand()")->first()->id,
            'category_id'=>Category::orderByRaw("rand()")->first()->id,
            'image_id'=>$this->faker->randomElement([8,9,10,11,12,13])
        ];
    }
}
