<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class AdminFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name'=>$this->faker->word(),
            'email'=>$this->faker->unique()->safeEmail(),
            'mobile' =>$this->faker->unique()->phoneNumber,
            'password' =>Hash::make("123123"),
            //'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',//password
            'status'=>$this->faker->word(),

            'gender'=>'M',



        ];
    }
}
