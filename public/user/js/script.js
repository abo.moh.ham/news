if($(window).width() > 767) {
    (new WOW).init();
}
$(document).on('click', '.close-hint', function(e){
   e.preventDefault();
   $(this).closest('.hint-bar').slideToggle();
});
function reChangeValues(futureTime){
    // Time left between future and current time in Seconds
    var timeLeft = Math.floor( (futureTime - new Date().getTime()) / 1000 );
    // Days left = time left / Seconds per Day
    var days =  Math.floor(timeLeft / 86400);
    // 86400 seconds per Day
    timeLeft -= days * 86400;
    // Hours left = time left / Seconds per Hour
    var hours = Math.floor(timeLeft / 3600) % 24;
    // 3600 seconds per Hour
    timeLeft -= hours * 3600;
    // Minutes left = time left / Minutes per Hour
    var min = Math.floor(timeLeft / 60) % 60;
    // 60 seconds per minute
    timeLeft -= min * 60;
    // Seconds Left
    var sec = timeLeft % 60;

    $('.count-item.seconds').text(sec);
    $('.count-item.minutes').text(min);
    $('.count-item.hours').text(hours);
    $('.count-item.days').text(days);
}
function countDownTimer(date) {
    var elem = $('#countDown');

    var futureTime = new Date(date).getTime();
    reChangeValues(futureTime);
    setInterval(function() {
        reChangeValues(futureTime);
    }, 1000);
}

// Enter date in this format: January 1, 2017 12:00:00
countDownTimer('January 23, 2021 19:17:00');

$('.scrollable-area, .fstResults').mCustomScrollbar({
    mouseWheelPixels: 280,
});
$('.courses-list-wrap').mCustomScrollbar({
    autoHideScrollbar: true,
    mouseWheelPixels: 280,
});

$(document).on('click', '.fstControls', function(e){
    setTimeout(function(){
        $('.fstResults').mCustomScrollbar();
    }, 300);
});

$(document).on('click', '.dropdown-menu-toggle', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $(this).closest('.header-item').find('.dropdown-menu-item').toggleClass('active');
});
$(document).on('click', '.mobile-notifications-toggle', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $('.mobile-notifications').toggleClass('active');
});
$(document).on('click', '.notifications-close', function(e){
    e.preventDefault();
    $('.mobile-notifications-toggle').toggleClass('active');
    $('.mobile-notifications').toggleClass('active');
});
$(document).on('click', '.drop-item a', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $(this).closest('li').find('.drop-menu').toggleClass('active');
});
$(document).on('click', '.show-pass', function(e){
    e.preventDefault();
    if($(this).hasClass('active')){
        $(this).removeClass('active');
        $(this).html('<svg id="Combined_Shape" data-name="Combined Shape" xmlns="http://www.w3.org/2000/svg" width="16" height="12.001" viewBox="0 0 16 12.001"> <path id="Combined_Shape-2" data-name="Combined Shape" d="M8,12A8.036,8.036,0,0,1,2.1,9.123,13.694,13.694,0,0,1,.6,7.2,10,10,0,0,1,.07,6.3a.672.672,0,0,1,0-.6,10,10,0,0,1,.526-.9A13.694,13.694,0,0,1,2.1,2.878,8.036,8.036,0,0,1,8,0a8.038,8.038,0,0,1,5.9,2.878,13.694,13.694,0,0,1,1.5,1.927,10,10,0,0,1,.526.9.668.668,0,0,1,0,.6,10,10,0,0,1-.526.9,13.694,13.694,0,0,1-1.5,1.927A8.038,8.038,0,0,1,8,12ZM8,1.333a6.766,6.766,0,0,0-4.93,2.456A12.358,12.358,0,0,0,1.716,5.529c-.108.167-.2.326-.289.472.081.142.178.3.289.472A12.358,12.358,0,0,0,3.069,8.211,6.766,6.766,0,0,0,8,10.667a6.766,6.766,0,0,0,4.931-2.456,12.29,12.29,0,0,0,1.353-1.739c.107-.166.2-.325.289-.472-.086-.148-.183-.307-.289-.472a12.29,12.29,0,0,0-1.353-1.739A6.766,6.766,0,0,0,8,1.333ZM8,8.667A2.667,2.667,0,1,1,10.667,6,2.67,2.67,0,0,1,8,8.667Zm0-4A1.333,1.333,0,1,0,9.334,6,1.335,1.335,0,0,0,8,4.667Z" transform="translate(0)"/> </svg>');
        $(this).closest('.form-group').find('input').attr('type', 'password');
    }else{
        $(this).addClass('active');
        $(this).html('<svg id="hide_pass" data-name="Component 73 – 1" xmlns="http://www.w3.org/2000/svg" width="24.504" height="24" viewBox="0 0 24.504 24"> <path id="Combined_Shape-2" data-name="Combined Shape-2" d="M303.871,1178.05l-6.164,6.159a1,1,0,0,1-1.414,0h0a1,1,0,0,1,0-1.414l9.6-9.6a1.015,1.015,0,0,1,.167-.167l12.233-12.233a1,1,0,1,1,1.414,1.415l-11.584,11.584Z" transform="translate(-295.496 -1160.502)"/> <path id="Path_17" data-name="Path 17" d="M303.5,1173a3.908,3.908,0,0,0,.054.532l1.839-1.839a1.017,1.017,0,0,1,.167-.167l2.472-2.472a3.948,3.948,0,0,0-.532-.054A4,4,0,0,0,303.5,1173Z" transform="translate(-295.496 -1160.502)"/> <path id="Path_15" data-name="Path 15" d="M296.395,1174.79a20.52,20.52,0,0,0,2.611,3.266h.02l1.411-1.411c-.112-.113-.226-.215-.337-.333a18.577,18.577,0,0,1-2.03-2.61c-.165-.256-.311-.494-.433-.707.126-.218.271-.456.433-.707a18.571,18.571,0,0,1,2.03-2.609c2.3-2.444,4.785-3.683,7.4-3.683a8.16,8.16,0,0,1,2.42.371,1.021,1.021,0,0,0,1.032-.237h0a1,1,0,0,0-.376-1.661A10.225,10.225,0,0,0,307.5,1164c-3.19,0-6.169,1.452-8.855,4.316a20.489,20.489,0,0,0-2.25,2.891,15.091,15.091,0,0,0-.79,1.345.981.981,0,0,0,0,.9A14.979,14.979,0,0,0,296.395,1174.79Z" transform="translate(-295.496 -1160.502)"/> <path id="Path_16" data-name="Path 16" d="M309.5,1173a2,2,0,0,1-3.674,1.09l-1.418,1.42a3.985,3.985,0,1,0,5.6-5.6l-1.419,1.419A1.994,1.994,0,0,1,309.5,1173Z" transform="translate(-295.496 -1160.502)"/> <path id="Path_18" data-name="Path 18" d="M318.606,1171.21a20.568,20.568,0,0,0-2.251-2.891,16.21,16.21,0,0,0-2.544-2.212l-1.436,1.436a13.959,13.959,0,0,1,2.525,2.143,18.365,18.365,0,0,1,2.03,2.61c.16.247.306.485.434.707-.127.219-.273.457-.434.707a18.431,18.431,0,0,1-2.03,2.609c-2.3,2.445-4.785,3.684-7.4,3.684a9.105,9.105,0,0,1-5.561-2.021l-1.423,1.422a11.111,11.111,0,0,0,6.984,2.6c3.19,0,6.169-1.452,8.855-4.317a20.559,20.559,0,0,0,2.251-2.891,14.809,14.809,0,0,0,.788-1.345,1,1,0,0,0,0-.9,14.836,14.836,0,0,0-.788-1.341Z" transform="translate(-295.496 -1160.502)"/> </svg>');
        $(this).closest('.form-group').find('input').attr('type', 'text');
    }
});
$(document).on('click', '.header-icon.menu', function(e){
    e.preventDefault();
    if($(window).width() > 767){
        $('.main-menu').addClass('active');
        $('.home-light').addClass('out');
    }else{
        $('.mobile-menu').addClass('active');
    }
});
$(document).on('click', '.mobile-menu-close', function(e){
    e.preventDefault();
    $('.mobile-menu').removeClass('active');
});
$(document).on('click', '.close-menu', function(e){
    e.preventDefault();
    $('.main-menu').removeClass('active');
    $('.home-light').removeClass('out');
});

if($(".join-form").length) {
    $(".join-form").validate({
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            phone: {
                required: true,
            },
            password: {
                required: true,
            },
        },
        errorPlacement: function (error, element) {
        },
        highlight: function (element) {
            $(element).addClass('error');
            if($(element).closest('.form-group').find('.error-txt').length){
                $(element).closest('.form-group').find('.error-txt').addClass('active');
            }
        },
        unhighlight: function (element) {
            $(element).removeClass('error');
            if($(element).closest('.form-group').find('.error-txt').length){
                $(element).closest('.form-group').find('.error-txt').removeClass('active');
            }
        },
        submitHandler: function (form) {
            form.submit();
            /* request and response of ajax form submit */
            // pre-submit callback
            function showRequest(formData, jqForm, options) {
                var queryString = $.param(formData);
                return true;
            }
            return false;
        }
    });
}
/*
if($('#phone').length) {
    $("#phone").intlTelInput({
        numberType: "MOBILE",
        preventInvalidNumbers: true,
        initialCountry: "sa",
        geoIpLookup: function (callback) {
            $.get('https://ipinfo.io', function () {
            }, "jsonp").always(function (resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        preferredCountries: ['SA', 'AE', 'QA', 'KW', 'OM', 'BH', 'YE', 'LB', 'IQ', 'SY', 'PS', 'JO', 'EG', 'SD'],
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
    });
}
*/
$('.file-input input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $('.file-input span').text(fileName);
});
init_course_carousel();
function init_course_carousel(){
    $('.courses-carousel').owlCarousel({
        loop: false,
        rewind: false,
        margin: 30,
        nav: true,
        dots: false,
        rtl: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            1000: {
                items: 4
            },
        },
        navText : ['<svg xmlns="http://www.w3.org/2000/svg" width="8" height="14" viewBox="0 0 8 14"> <path id="Shape" d="M2.414,7l5.293,5.293a1,1,0,1,1-1.414,1.414l-6-6a1,1,0,0,1,0-1.414l6-6A1,1,0,0,1,7.707,1.707Z" transform="translate(0)" fill="#fff"/> </svg> ','<svg xmlns="http://www.w3.org/2000/svg" width="8" height="14" viewBox="0 0 8 14"> <path id="Shape" d="M2.414,7l5.293,5.293a1,1,0,1,1-1.414,1.414l-6-6a1,1,0,0,1,0-1.414l6-6A1,1,0,0,1,7.707,1.707Z" transform="translate(0)" fill="#fff"/> </svg> '],
        afterAction: function(){
            if ( this.itemsAmount > this.visibleItems.length ) {
                $('.next').show();
                $('.prev').show();

                $('.next').removeClass('disabled');
                $('.prev').removeClass('disabled');
                if ( this.currentItem == 0 ) {
                    $('.prev').addClass('disabled');
                }
                if ( this.currentItem == this.maximumItem ) {
                    $('.next').addClass('disabled');
                }

            } else {
                $('.next').hide();
                $('.prev').hide();
            }
        }
    });
}
$('.trainers-carousel').owlCarousel({
    loop: false,
    rewind: false,
    margin: 30,
    nav: true,
    dots: false,
    rtl: true,
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 2
        },
        768: {
            items: 3
        },
        1000: {
            items: 4
        },
    },
    navText : ['<svg xmlns="http://www.w3.org/2000/svg" width="8" height="14" viewBox="0 0 8 14"> <path id="Shape" d="M2.414,7l5.293,5.293a1,1,0,1,1-1.414,1.414l-6-6a1,1,0,0,1,0-1.414l6-6A1,1,0,0,1,7.707,1.707Z" transform="translate(0)" fill="#fff"/> </svg> ','<svg xmlns="http://www.w3.org/2000/svg" width="8" height="14" viewBox="0 0 8 14"> <path id="Shape" d="M2.414,7l5.293,5.293a1,1,0,1,1-1.414,1.414l-6-6a1,1,0,0,1,0-1.414l6-6A1,1,0,0,1,7.707,1.707Z" transform="translate(0)" fill="#fff"/> </svg> '],
});

$('.testimonial-carousel').owlCarousel({
    loop: false,
    rewind: false,
    margin: 0,
    nav: false,
    dots: false,
    rtl: true,
    items: 1,
    dotsContainer: '.testim-dots'
});
$('.testim-dot').click(function () {
    $('.testim-dot').removeClass('active');
    $(this).addClass('active');
    $('.testimonial-carousel').trigger('to.owl.carousel', [$(this).index(), 300]);
});
$('.testimonial-carousel').on('translate.owl.carousel', function(e){
    idx = e.item.index;
    $('.testim-dot').removeClass('active');
    $('.testim-dot').eq(idx).addClass('active');
});
$('.country-list').mCustomScrollbar();

$(document).on('click', '.show-more-cats', function(e){
    e.preventDefault();
    $(this).hide();
    $(this).closest('.filter-item-content').find('.input-filter-item').fadeIn();
});
$(document).on('click', '.mobile-open-filter', function(e){
    e.preventDefault();
    $('.courses-filter-wrap').addClass('active');
});
$(document).on('click', '.close-filter, .submit-filter', function(e){
    e.preventDefault();
    $('.courses-filter-wrap').removeClass('active');
});
$(document).on('click', '.mobile-search-type', function(e){
    e.preventDefault();
    $('.seach-filters').toggleClass('active');
});
$(document).on('click', '.remove-cart', function(e){
    e.preventDefault();
    $(this).closest('.cart-item').fadeOut(300, function(){
        $(this).remove()
    });
});
$(document).on('click', '.selected-item-toggle', function(e){
    e.preventDefault();
    $(this).closest('.select-input-wrap').find('.select-item-list').toggleClass('active');
});
$(document).on('click', '.select-item-list a', function(e){
    e.preventDefault();
    $('.select-item-list a').removeClass('active');
    $(this).closest('.select-input-wrap').find('.selected-item-toggle').removeClass('placeholder').find('span').text($(this).text());
    $(this).closest('.select-item-list').removeClass('active');
    $(this).closest('.select-input-wrap').find('.expire-value').val($(this).text());
    $(this).addClass('active');
    $(this).closest('.select-input-wrap').find('.account-name').text($(this).attr('data-name'));
    $(this).closest('.select-input-wrap').find('.account-number').text($(this).attr('data-number'));
    $(this).closest('.select-input-wrap').find('.account-iban').text($(this).attr('data-iban'));
});
$(document).on('click', '.seach-filters a', function(e){
    e.preventDefault();
    $('.seach-filters a').removeClass('active');
    $(this).addClass('active');
    $('.mobile-search-type span').text($(this).text());
    $(this).closest('form').find('#search_type').val($(this).attr('data-type'));
    $('.seach-filters').removeClass('active');
});
$(document).on('click', '.follow-link', function(e){
    e.preventDefault();
    var $count = parseInt($(this).closest('.trainer-big-item').find('.followers-count b').text());
    if($(this).hasClass('followed')){
        $(this).find('.followed-icon').hide();
        $(this).find('.follow-icon').show();
        $(this).removeClass('followed');
        $(this).find('span').text($(this).attr('data-follow'));
        $(this).closest('.trainer-big-item').find('.followers-count b').text($count-1);
    }else{
        $(this).find('.followed-icon').show();
        $(this).find('.follow-icon').hide();
        $(this).addClass('followed');
        $(this).find('span').text($(this).attr('data-followed'));
        $(this).closest('.trainer-big-item').find('.followers-count b').text($count+1);
    }
});
$(document).on('click', '.login-action', function(e){
    e.preventDefault();
    $('.wizard-wrap.register-section').hide();
    $('.wizard-wrap.login-section').fadeIn();
});
$(document).on('click', '.register-action', function(e){
    e.preventDefault();
    $('.wizard-wrap.login-section').hide();
    $('.wizard-wrap.register-section').fadeIn();
});
$(document).on('click', '.order-details-wrap .cart-title', function(e){
    e.preventDefault();
    if($(window).width() < 768) {
        $('.cart-details-wrap').slideToggle();
    }
});
// $(document).on('click', '.more-content-items', function(e){
//     e.preventDefault();
//     $(this).closest('.course-overview-section').find('.hidden-item').fadeIn();
//     $(this).remove();
// });
var connectSlider2 = document.getElementById('range-slider');
if($('.range-slider:not(.crop)').length > 0) {
    noUiSlider.create(connectSlider2, {
        start: [300, 1200],
        direction: 'rtl',
        connect: [false, true, false],
        step: 1,
        range: {
            'min': 0,
            'max': 3500
        }
    });
    connectSlider2.noUiSlider.on('update', function (values, handle) {
        $('.first-val').text(values[0].split('.')[0]);
        $('.last-val i').text(values[1].split('.')[0]);
        $('.price-filter-input').val(values[0].split('.')[0]+','+values[1].split('.')[0]);
    });
}

if($('#otp-countdown').length){
    var timer2 = "1:00";
    var interval = setInterval(function() {
        var timer = timer2.split(':');
        //by parsing integer, I avoid all extra string processing
        var minutes = parseInt(timer[0], 10);
        var seconds = parseInt(timer[1], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes < 0) clearInterval(interval);
        seconds = (seconds < 0) ? 59 : seconds;
        seconds = (seconds < 10) ? '0' + seconds : seconds;
        //minutes = (minutes < 10) ?  minutes : minutes;
        $('#otp-countdown').html(minutes + ':' + seconds);
        if(seconds == 0 && minutes == 0){
            $('#otp-countdown').hide();
            $('.resent-otp').addClass('active');
        }
        timer2 = minutes + ':' + seconds;
    }, 1000);
}

var elementPosition =  $('.course-overview-nav');
var elementPosition2 =  $('footer');
if(elementPosition.length){
    elementPosition = $('.course-overview-nav').offset();
    elementPosition2 =  $('footer').offset();
    $(window).scroll(function(){
        if ($(window).scrollTop() > elementPosition.top && $(window).scrollTop() < elementPosition2.top) {
            $('.course-overview-nav').addClass('fixed');
            if($(window).width() > 767) {
                $('.course-overview-sections-wrap').addClass('active');
            }
        } else {
            $('.course-overview-nav').removeClass('fixed');
            if($(window).width() > 767) {
                $('.course-overview-sections-wrap').removeClass('active');
            }
        }
    });
}
$(document).on('click', '.toggle-course-menu', function(e){
   e.preventDefault();
   $(this).toggleClass('active');
   $(this).closest('.course-menu-wrap').find('.course-drop-menu').toggleClass('active');
});
$(document).on('click', '.change-cover-image', function(e){
   e.preventDefault();
   $('.cover-img').attr('data-type', 'cover').click();
});
$(document).on('click', '.change-profile-image', function(e){
    e.preventDefault();
    $('.cover-img').attr('data-type', 'profile').click();
});
$(document).on('click', '.info-change-pass', function(e){
   e.preventDefault();
    $('.main-info').hide();
    $('.change-pass-form').fadeIn();
});
$(document).on('click', '.change-pass-back', function(e){
   e.preventDefault();
    $('.change-pass-form').hide();
    $('.main-info').fadeIn();
});
$(document).on('change', '.course-choose-item', function(e){
    if($(this).is(':checked')){
        $(this).closest('.course-item').addClass('checked');
    }else{
        $(this).closest('.course-item').removeClass('checked');
    }
});
$(document).on('click', '.choose-course', function(e){
    e.preventDefault();
    $('.courses-gifts-wrap').hide();
    $('.choose-course-wrap').fadeIn();
});
$(document).on('click', '.open-course-lessons', function(e){
    e.preventDefault();
    $(this).addClass('hidden');
    $('.course-view-section').addClass('active');
    $('.course-content-wrap').addClass('active');
});
$(document).on('click', '.close-content', function(e){
    e.preventDefault();
    $('.open-course-lessons').removeClass('hidden');
    $('.course-view-section').removeClass('active');
    $('.course-content-wrap').removeClass('active');
});

$(".star-select-item").bind('mouseover',function(event){
    $(this).addClass('hover');
    $(this).prevAll().addClass('hover');
});
$(".star-select-item").bind('mouseleave', function(e) {
    $(this).removeClass('hover');
    $(this).prevAll().removeClass('hover');
});
$(document).on('click', '.star-select-item', function(e){
    e.preventDefault();
    $('.star-select-item').removeClass('active');
    $(this).addClass('active');
    $(this).prevAll().addClass('active');
    $('.stars-number').val($(this).prevAll().length + 1);
});
$(document).on('click', '.add-reply', function(e){
    e.preventDefault();
    $(this).closest('.reply-wrap').find('.add-rate-comment').slideToggle();
});

if($(window).width() < 991) {
    $('.list-link-mobile a').click();
}
if($('.counter').length){
    $(window).scroll(testScroll);
    $(document).ready(testScroll);
    var viewed = false;

    function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    function testScroll() {
        if (isScrolledIntoView($(".counter")) && !viewed) {
            viewed = true;
            $('.counter').each(function () {
                $(this).prop('Counter',$(this).attr('data-from')).animate({
                    Counter: $(this).attr('data-to')
                }, {
                    duration: 2000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        }
    }
}

$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $('.main-menu, .dropdown-menu-toggle, .select-item-list, .dropdown-menu-item, .drop-menu, .drop-item a, .toggle-course-menu, .course-drop-menu').removeClass('active');
    }
});

$(document).click(function(event) {
    $target = $(event.target);
    if(!$target.closest('.user-login-menu').length &&
        $('.user-login-menu').hasClass('active') && !$target.closest('.dropdown-menu-toggle.login').length) {
        $('.user-login-menu').removeClass('active');
        $('.dropdown-menu-toggle.login').removeClass('active');
    }
});
$(document).click(function(event) {
    if($(window).width() > 768) {
        $target = $(event.target);
        if (!$target.closest('.notifications-menu').length &&
            $('.notifications-menu').hasClass('active') && !$target.closest('.dropdown-menu-toggle.notifications').length) {
            $('.notifications-menu').removeClass('active');
            $('.dropdown-menu-toggle.notifications').removeClass('active');
        }
    }
});
$(document).click(function(event) {
    $target = $(event.target);
    if(!$target.closest('.drop-menu').length &&
        $('.drop-menu').hasClass('active') && !$target.closest('.drop-item a').length) {
        $('.drop-menu').removeClass('active');
        $('.drop-item a').removeClass('active');
    }
});
$(document).click(function(event) {
    $target = $(event.target);
    if(!$target.closest('.course-drop-menu').length &&
        $('.course-drop-menu').hasClass('active') && !$target.closest('.toggle-course-menu').length) {
        $('.course-drop-menu').removeClass('active');
        $('.toggle-course-menu').removeClass('active');
    }
});
$(document).click(function(event) {
    $target = $(event.target);
    if(!$target.closest('.select-item-list.country').length &&
        $('.select-item-list.country').hasClass('active') && !$target.closest('.selected-item-toggle.country').length) {
        $('.select-item-list.country').removeClass('active');
        $('.selected-item-toggle.country').removeClass('active');
    }
});
$(document).click(function(event) {
    $target = $(event.target);
    if(!$target.closest('.select-item-list.city').length &&
        $('.select-item-list.city').hasClass('active') && !$target.closest('.selected-item-toggle.city').length) {
        $('.select-item-list.city').removeClass('active');
        $('.selected-item-toggle.city').removeClass('active');
    }
});

var $html = '<div class="courses-carousel owl-carousel owl-theme wow fadeIn"> <div class="course-item"> <div class="course-img"> <a href="#"><img src="images/course5.png" /></a> </div> <div class="course-content"> <h3><a href="#">ابتكار المشاريع الريادية المربحة المتقدمة</a></h3> <div class="course-rate"> <div class="stars"> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_1"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_1) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_2"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_2) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_3"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_3) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_4"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_4) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_5"> <stop offset="70%" stop-color="#E4EAF1"></stop> <stop offset="30%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_5) "></path> </svg> </div> <span class="rating-text">4.3</span> <span class="rating-number">(452)</span> </div> <div class="course-footer"> <div class="price"> <span>‏332 ر.س</span> <span class="dashed">‏532 ر.س</span> </div> <div class="type">للشركات</div> </div> </div> </div> <div class="course-item"> <div class="course-img"> <a href="#"><img src="images/course6.png" /></a> </div> <div class="course-content"> <h3><a href="#">ابتكار المشاريع الريادية المربحة المتقدمة</a></h3> <div class="course-footer"> <div class="price"> <span>‏332 ر.س</span> <span class="dashed">‏532 ر.س</span> </div> <div class="type">للشركات</div> </div> </div> </div> <div class="course-item"> <div class="course-img"> <a href="#"><img src="images/course3.png" /></a> </div> <div class="course-content"> <h3><a href="#">ابتكار المشاريع الريادية </a></h3> <div class="course-rate"> <div class="stars"> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_1"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_1) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_2"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_2) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_3"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_3) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_4"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_4) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_5"> <stop offset="70%" stop-color="#E4EAF1"></stop> <stop offset="30%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_5) "></path> </svg> </div> <span class="rating-text">4.3</span> <span class="rating-number">(452)</span> </div> <div class="course-footer"> <div class="price"> <span>‏332 ر.س</span> <span class="dashed">‏532 ر.س</span> </div> <div class="type">للشركات</div> </div> </div> </div> <div class="course-item"> <div class="course-img"> <a href="#"><img src="images/course8.png" /></a> </div> <div class="course-content"> <h3><a href="#">ابتكار المشاريع الريادية المربحة المتقدمة</a></h3> <div class="course-rate"> <div class="stars"> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_1"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_1) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_2"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_2) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_3"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_3) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_4"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_4) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_5"> <stop offset="70%" stop-color="#E4EAF1"></stop> <stop offset="30%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_5) "></path> </svg> </div> <span class="rating-text">4.3</span> <span class="rating-number">(452)</span> </div> <div class="course-footer"> <div class="price"> <span>‏332 ر.س</span> <span class="dashed">‏532 ر.س</span> </div> <div class="type">للشركات</div> </div> </div> </div> <div class="course-item"> <div class="course-img"> <a href="#"><img src="images/course7.png" /></a> </div> <div class="course-content"> <h3><a href="#">ابتكار المشاريع الريادية المربحة المتقدمة</a></h3> <div class="course-rate"> <div class="stars"> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_1"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_1) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_2"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_2) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_3"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_3) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_4"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_4) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_5"> <stop offset="70%" stop-color="#E4EAF1"></stop> <stop offset="30%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_5) "></path> </svg> </div> <span class="rating-text">4.3</span> <span class="rating-number">(452)</span> </div> <div class="course-footer"> <div class="price"> <span>‏332 ر.س</span> <span class="dashed">‏532 ر.س</span> </div> <div class="type">للشركات</div> </div> </div> </div> </div>';
$(document).on('click', '.course-tab-item', function(e){
   e.preventDefault();
   if(!$(this).hasClass('active')){
       $('.courses-tabs-wrap a').removeClass('active');
       $(this).addClass('active');
       var $cat = $(this).attr('data-cat');
       // get data based on category
       $('.course-loader').show();
       $('.course-tabs-content').html('');
       // replace timeout with ajax request
       setTimeout(function(){
           $('.course-loader').hide();
           $('.course-tabs-content').html($html);
           init_course_carousel();
       }, 1000);
   }
});

var $html2 = '<div class="col-6 col-lg-4"> <div class="course-item" data-wow-delay="0.2s"> <div class="course-img"> <a href="#"><img src="images/course6.png" /></a> </div> <div class="course-content"> <h3><a href="#">ابتكار المشاريع الريادية المربحة المتقدمة</a></h3> <div class="course-footer"> <div class="price"> <span>‏332 ر.س</span> <span class="dashed">‏532 ر.س</span> </div> <div class="type">للشركات</div> </div> </div> </div> </div> <div class="col-6 col-lg-4"> <div class="course-item " data-wow-delay="0.4s"> <div class="course-img"> <a href="#"><img src="images/course2.png" /></a> </div> <div class="course-content"> <h3><a href="#">ابتكار المشاريع الريادية المربحة المتقدمة</a></h3> <div class="course-footer"> <div class="price"> <span>‏332 ر.س</span> <span class="dashed">‏532 ر.س</span> </div> <div class="type">للشركات</div> </div> </div> </div> </div>'
$(document).on('click', '.course-search-tab-item', function(e){
   e.preventDefault();
   if(!$(this).hasClass('active')){
       $('.courses-tabs-wrap a').removeClass('active');
       $(this).addClass('active');
       var $cat = $(this).attr('data-cat');
       // get data based on category
       $('.course-loader').addClass('show').show();
       $('.course-search-items').html('');
       // replace timeout with ajax request
       setTimeout(function(){
           $('.course-loader').hide().removeClass('show');
           $('.course-search-items').html($html2+$html2+$html2+$html2+$html2+$html2+$html2+$html2+$html2+$html2);
       }, 1000);
   }
});

var $html3 = '<div class="comment-item"> <div class="comment-header"> <div class="img"><img src="images/user.png" /></div> <div class="meta"> <h3>محمد عبد الرحمن</h3> <span class="time">‏22 مارس 2020</span> </div> </div> <div class="comment-text">اتت مبادرة تقديم هذه الدورة بعد الاوضاع الحالية التي رأينها ومن واقع خبرتي بالعمل عن بعد لأكثر من 10 سنوات .قررت أن اقدم هذه الدورة التي سأعلمكم فيها اساسيات مهمة جدا</div> </div> <div class="comment-item answer"> <div class="comment-header"> <div class="img"><img src="images/user.png" /></div> <div class="meta"> <h3>محمد عبد الرحمن</h3> <span class="time">‏22 مارس 2020</span> </div> </div> <div class="comment-text">اتت مبادرة تقديم هذه الدورة بعد الاوضاع الحالية التي رأينها</div> </div>';
$(document).on('click', '.more-questions', function(e){
   e.preventDefault();
   var $this = $(this);
   // get data based on category
    $this.closest('.comments-wrapper').find('.comments-loader').addClass('show').show();
   // replace timeout with ajax request
   setTimeout(function(){
       $this.closest('.comments-wrapper').find('.comments-loader').hide().removeClass('show');
       $('.comments-items-wrapper').append($html3);
       $('[data-spy="scroll"]').each(function () {
           var $spy = $(this).scrollspy('refresh')
       })
   }, 1000);
});

var $html4 = '<div class="comment-item"> <div class="comment-header"> <span class="review-time">منذ أسبوع</span> <div class="img"><img src="images/user.png" /></div> <div class="meta"> <h3>محمد عبد الرحمن</h3> <div class="stars"> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_1"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_1) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_2"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_2) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_3"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_3) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_4"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_4) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_5"> <stop offset="100%" stop-color="#E4EAF1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_5) "></path> </svg> </div> </div> </div> <div class="comment-text">اتت مبادرة تقديم هذه الدورة بعد الاوضاع الحالية التي رأينها ومن واقع خبرتي بالعمل عن بعد لأكثر من 10 سنوات .قررت أن اقدم هذه الدورة التي سأعلمكم فيها اساسيات مهمة جدا</div> </div><div class="comment-item"> <div class="comment-header"> <span class="review-time">منذ أسبوع</span> <div class="img"><img src="images/user.png" /></div> <div class="meta"> <h3>محمد عبد الرحمن</h3> <div class="stars"> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_1"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_1) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_2"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_2) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_3"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_3) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_4"> <stop offset="100%" stop-color="#fecd73" stop-opacity="1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_4) "></path> </svg> <svg id="Shape" xmlns="http://www.w3.org/2000/svg" width="12.002" height="11.022" viewBox="0 0 12.002 11.022"> <defs> <linearGradient id="half_grad_5"> <stop offset="100%" stop-color="#E4EAF1"></stop> </linearGradient> </defs> <path d="M9.118,10.961,6,9.385,2.884,10.961a.541.541,0,0,1-.791-.553l.6-3.336L.165,4.712a.52.52,0,0,1,.3-.895l3.486-.489L5.512.292a.556.556,0,0,1,.978,0L8.049,3.328l3.486.489a.52.52,0,0,1,.3.895L9.314,7.072l.6,3.336a.532.532,0,0,1-.537.614A.56.56,0,0,1,9.118,10.961Z" fill="url(#half_grad_5) "></path> </svg> </div> </div> </div> <div class="comment-text">اتت مبادرة تقديم هذه الدورة بعد الاوضاع الحالية التي رأينها ومن واقع خبرتي بالعمل عن بعد لأكثر من 10 سنوات .قررت أن اقدم هذه الدورة التي سأعلمكم فيها اساسيات مهمة جدا</div> </div>';
$(document).on('click', '.more-rates', function(e){
   e.preventDefault();
   var $this = $(this);
   // get data based on category
    $this.closest('.comments-wrapper').find('.comments-loader').addClass('show').show();
   // replace timeout with ajax request
   setTimeout(function(){
       $this.closest('.comments-wrapper').find('.comments-loader').hide().removeClass('show');
       $('.comments-items-wrapper').append($html4);
       $('[data-spy="scroll"]').each(function () {
           var $spy = $(this).scrollspy('refresh')
       })
   }, 1000);
});

var $html5 = '<div class="course-file-item"> <h3>كيف تحول مشروعك الى مشروع تقني؟</h3> <div class="meta"> <span>pdf ملف</span> <span>3 MB</span> </div> <a href="#" class="download-file"> <svg id="Combined_Shape" data-name="Combined Shape" xmlns="http://www.w3.org/2000/svg" width="16" height="18" viewBox="0 0 16 18"> <path id="Combined_Shape-2" data-name="Combined Shape" d="M9.6,18a.818.818,0,0,1,0-1.636h4a.809.809,0,0,0,.8-.818V2.454a.809.809,0,0,0-.8-.818h-4A.809.809,0,0,1,8.8.818.809.809,0,0,1,9.6,0h4A2.427,2.427,0,0,1,16,2.454V15.545A2.427,2.427,0,0,1,13.6,18ZM6.634,12.851a.831.831,0,0,1,0-1.157L8.469,9.818H.8a.818.818,0,0,1,0-1.636H8.469L6.634,6.305a.831.831,0,0,1,0-1.157.788.788,0,0,1,1.132,0L10.95,8.406a.831.831,0,0,1,.095,1.078h0l-.012.016h0l-.012.016h0l-.013.016h0l-.013.015h0l-.028.03h0l-3.2,3.272a.788.788,0,0,1-1.132,0Z" fill="#fff"/> </svg> <span>تحميل</span> </a> </div> <div class="course-file-item"> <h3>كيف تحول مشروعك الى مشروع تقني؟</h3> <div class="meta"> <span>pdf ملف</span> <span>3 MB</span> </div> <a href="#" class="download-file"> <svg id="Combined_Shape" data-name="Combined Shape" xmlns="http://www.w3.org/2000/svg" width="16" height="18" viewBox="0 0 16 18"> <path id="Combined_Shape-2" data-name="Combined Shape" d="M9.6,18a.818.818,0,0,1,0-1.636h4a.809.809,0,0,0,.8-.818V2.454a.809.809,0,0,0-.8-.818h-4A.809.809,0,0,1,8.8.818.809.809,0,0,1,9.6,0h4A2.427,2.427,0,0,1,16,2.454V15.545A2.427,2.427,0,0,1,13.6,18ZM6.634,12.851a.831.831,0,0,1,0-1.157L8.469,9.818H.8a.818.818,0,0,1,0-1.636H8.469L6.634,6.305a.831.831,0,0,1,0-1.157.788.788,0,0,1,1.132,0L10.95,8.406a.831.831,0,0,1,.095,1.078h0l-.012.016h0l-.012.016h0l-.013.016h0l-.013.015h0l-.028.03h0l-3.2,3.272a.788.788,0,0,1-1.132,0Z" fill="#fff"/> </svg> <span>تحميل</span> </a> </div>';
$(document).on('click', '.more-files', function(e){
   e.preventDefault();
   var $this = $(this);
   // get data based on category
    $this.closest('.course-overview-section').find('.bordered-loader').addClass('show').show();
   // replace timeout with ajax request
   setTimeout(function(){
       $this.closest('.course-overview-section').find('.bordered-loader').hide().removeClass('show');
       $('.files-items-wrapper').append($html5);
       $('[data-spy="scroll"]').each(function () {
           var $spy = $(this).scrollspy('refresh')
       })
   }, 1000);
});

var $html6 = '<div class="course-content-item"> <span>الفيديو التعريفي و الترحيبي بالدورة</span> </div> <div class="course-content-item"> <span>قصتي مع العمل عن بعد</span> </div> <div class="course-content-item"> <span>مشروعك خدمات ولا منتجات ؟</span> </div>';
$(document).on('click', '.more-course-items', function(e){
   e.preventDefault();
   var $this = $(this);
   // get data based on category
    $this.closest('.course-overview-section').find('.bordered-loader').addClass('show').show();
   // replace timeout with ajax request
   setTimeout(function(){
       $this.closest('.course-overview-section').find('.bordered-loader').hide().removeClass('show');
       $('.course-contents-wrapper').append($html6);
       $('[data-spy="scroll"]').each(function () {
           var $spy = $(this).scrollspy('refresh')
       })
   }, 1000);
});

function toastMessage($type, $message){
    $('.toast_messages').addClass('active');
    var $html = '<div class="toast_message_item"><div class="toast_message '+$type+'">' +
        $message +
        '</div></div>';
    $('.toast_messages').append($html);
}
$(document).on('click', '.toast_message', function(){
    $(this).closest('.toast_message_item').slideToggle(300, function(){
        $(this).closest('.toast_message_item').remove()
    });
});
toastMessage('success', 'خطأ في اسم المستخدم أو كلمة المرور');
toastMessage('success', 'خطأ في اسم المستخدم أو كلمة المرور');
toastMessage('error', 'خطأ في اسم المستخدم أو كلمة المرور');

setInterval(function(){
    if($('.toast_message_item').length){
        $('.toast_message_item').each(function(){
            var $this = $(this);
            setTimeout(function(){
                $this.slideToggle(300, function(){
                    $(this).remove()
                });
            }, 4000)
        });
    }else{
        $('.toast_messages').removeClass('active');
    }
}, 300);

//$('form').find('.submit-button').append('<div class="btn-loader"></div>');

$(document).ready(function () {
    $(".rating-stars input:radio").attr("checked", false);
    $('.rating-stars input').click(function () {
        $(".rating-stars span").removeClass('checked');
        $(this).parent().addClass('checked');
        $(this).parent().prevAll().addClass('active');
        $(this).parent().nextAll().removeClass('active');
    });
    $(".rating-stars span").mouseover(function () {
        $(this).addClass('active');
        $(this).prevAll().addClass('active');
    });
    $(".rating-stars span").mouseout(function () {
        if ($(".rating-stars span").hasClass('checked')) {
            $(".rating-stars span.checked").nextAll().removeClass('active');
        } else {
            $(".rating-stars span").removeClass('active');
        }
    });
});


$(document).on('click', '.faq-question h3', function(e){
    e.preventDefault();
    var $this = $(this).closest('.faq-question');
    $this.toggleClass('active');
});
$(document).on('click', '.qusestions-category h3', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $(this).closest('.qusestions-category').find('.faq-cats-wrapper').toggleClass('active');
});
$(document).on('click', '.help-toggle', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $('.help-wrapper').toggleClass('active');
    if($(window).width() < 767) {
        $('body').toggleClass('faq-active');
    }
});
$(document).on('click', '.faq-cat-item', function(e){
    e.preventDefault();
    $('.faq-cat-item').removeClass('active');
    $(this).addClass('active');
    $("html, body").animate({ scrollTop: $($(this).attr('href')).offset().top - 20 }, 100);
});

var $faq_cat =  $('.qusestions-category');
var $footer =  $('footer');
if($faq_cat.length){
    $faq_cat = $('.qusestions-category').offset();
    $footer =  $('footer').offset();
    $(window).scroll(function(){
        if($(window).width() > 767) {
            if ($(window).scrollTop() > $faq_cat.top && $(window).scrollTop() < $footer.top) {
                $('.qusestions-category').addClass('fixed');
            } else {
                $('.qusestions-category').removeClass('fixed');
            }
        }
    });
}

$(document).on('click', '.help-actions .next', function(e){
    e.preventDefault();
    var $next = $('.help-items').find('.help-item.active').next();
    if($next.length){
        $('.help-item').hide().removeClass('active');
        $next.fadeIn().addClass('active');
        $('.help-actions .dot-item').removeClass('active');
        $('.help-actions .dot-item').eq($next.index()).addClass('active');
    }
});
$(document).on('click', '.help-actions .prev', function(e){
    e.preventDefault();
    var $prev = $('.help-items').find('.help-item.active').prev();
    if($prev.length){
        $('.help-item').hide().removeClass('active');
        $prev.fadeIn().addClass('active');
        $('.help-actions .dot-item').removeClass('active');
        $('.help-actions .dot-item').eq($prev.index()).addClass('active');
    }
});
$(window).scroll(function(){
    scrollCheck();
});
$(document).ready(function(){
    scrollCheck();
});
function scrollCheck() {
    if($(window).width() > 767) {
        if (isScrolledIntoView('.faq-package:eq(0)')) {
            $('.faq-cat-item-wrap a').removeClass('active');
            $('.faq-cat-item-wrap a:eq(0)').addClass('active');
        }
        if (isScrolledIntoView('.faq-package:eq(1)')) {
            $('.faq-cat-item-wrap a').removeClass('active');
            $('.faq-cat-item-wrap a:eq(1)').addClass('active');
        }
        if (isScrolledIntoView('.faq-package:eq(2)')) {
            $('.faq-cat-item-wrap a').removeClass('active');
            $('.faq-cat-item-wrap a:eq(02)').addClass('active');
        }
        if (isScrolledIntoView('.faq-package:eq(3)')) {
            $('.faq-cat-item-wrap a').removeClass('active');
            $('.faq-cat-item-wrap a:eq(3)').addClass('active');
        }
        if (isScrolledIntoView('.faq-package:eq(4)')) {
            $('.faq-cat-item-wrap a').removeClass('active');
            $('.faq-cat-item-wrap a:eq(4)').addClass('active');
        }
        if (isScrolledIntoView('.faq-package:eq(5)')) {
            $('.faq-cat-item-wrap a').removeClass('active');
            $('.faq-cat-item-wrap a:eq(5)').addClass('active');
        }
        if (isScrolledIntoView('.faq-package:eq(6)')) {
            $('.faq-cat-item-wrap a').removeClass('active');
            $('.faq-cat-item-wrap a:eq(6)').addClass('active');
        }
        if (isScrolledIntoView('.faq-package:eq(7)')) {
            $('.faq-cat-item-wrap a').removeClass('active');
            $('.faq-cat-item-wrap a:eq(7)').addClass('active');
        }
        if (isScrolledIntoView('.faq-package:eq(8)')) {
            $('.faq-cat-item-wrap a').removeClass('active');
            $('.faq-cat-item-wrap a:eq(8)').addClass('active');
        }
        if (isScrolledIntoView('.faq-package:eq(9)')) {
            $('.faq-cat-item-wrap a').removeClass('active');
            $('.faq-cat-item-wrap a:eq(9)').addClass('active');
        }
    }
}
function isScrolledIntoView(elem){
    if($(elem).length){
        var hT = $(elem).offset().top,
            hH = $(elem).outerHeight(),
            wH = $(window).height(),
            wS = $(window).scrollTop();
        var $next = $(elem).next();
        var $cond = wS > (hT);
        if($next.length){
            $next = $(elem).next().offset().top;
            $cond = wS > (hT) && wS < $next
        }
        if ($cond) {
            return true;
        }else{
            return false;
        }
    }

}