<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminPermissionController;
use App\Http\Controllers\articlesController;
use App\Http\Controllers\AuthorAuthController;
use App\Http\Controllers\Cms\Auth\LoginController as AuthLoginController;
use App\Http\Controllers\AuthorAuthControole;
use App\Http\Controllers\authorController;
use App\Http\Controllers\categoryController;
use App\Http\Controllers\Cms\Auth\LoginController;
use App\Http\Controllers\Cms\HomeController as CmsHomeController;
use App\Http\Controllers\commentController;
use App\Http\Controllers\contactRequestController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RolePermissionController;
use App\Http\Controllers\SearchConrtoller;
use App\Http\Controllers\settingController;
use App\Http\Controllers\UserAuthController;
use App\Http\Controllers\UserController;
use App\Models\ContactRequest;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'index'])->name('home');

Route::get('/blog/{id}',  [HomeController::class,'show'])
->name('home-article');

Route::get('/author/{id}',  [HomeController::class,'showAuthor'])
->name('home-author');
Route::get('/category/{id}',  [HomeController::class,'showCategory'])
->name('home-category');

Route::post('/blog/{id}/comment',[HomeController::class,'StorCommment'])->name('comments-user')->middleware('auth:user');

// Route::get('/employee',[SearchConrtoller::class,'index']);
Route::post('/search',[SearchConrtoller::class,'showSearch'])->name('search');

// Route::prefix('cms/admin')->name('cms.')->middleware('guest:admin')->group(function(){
//     Route::get('login',[LoginController::class,'showLoginForm'])->name('login');
//     Route::post('login',[LoginController::class,'login'])->name('login');
//     // Auth::routes();
// });



Route::prefix('cms/author')->middleware('guest:author')->group(function () {
    Route::get('login', [AuthorAuthController::class, 'showLogin'])->name('auth-author.login.view');
    Route::post('login', [AuthorAuthController::class, 'login'])->name('auth-author.login');
});



Route::prefix('user')->middleware('guest:user')->group(function () {
    Route::get('login', [UserAuthController::class, 'showLogin'])->name('auth-user.login.view');
    Route::post('login', [UserAuthController::class, 'login'])->name('auth-user.login');
});




Route::get('cms/dashboard',[CmsHomeController::class,'index'])->name('cms.dashboard')
->middleware('auth:admin,author');




Route::prefix('cms/admin')->name('admin.')->middleware('guest:admin')->group(function(){
    Route::get('login',[AuthLoginController::class,'showLoginForm'])->name('auth.login.view');
    Route::post('login',[AuthLoginController::class,'login'])->name('login');
    // Auth::routes();
});



Route::get('logout', [UserAuthController::class, 'logout'])->name('auth-user.logout')->middleware('auth:user');




Route::prefix('cms')->middleware('auth:admin,author')->group(function(){
Route::resource('admins', AdminController::class);
// Route::view('', 'cms.dashboard')->name('cms.dashboard');
Route::resource('contacts', contactRequestController::class);
Route::resource('comments', commentController::class);


Route::resource('roles', RoleController::class);
Route::resource('permissions', PermissionController::class);

Route::resource('admins.permissions', AdminPermissionController::class);
Route::resource('role.permissions', RolePermissionController::class);

Route::resource('settings', settingController::class)->only(['index','store']);
Route::resource('users', UserController::class);
Route::resource('authors', authorController::class);
Route::resource('categories', categoryController::class);
Route::resource('articles', articlesController::class);


Route::get('edit-password', [AuthorAuthController::class, 'editPassword'])->name('auth.edit-password');
Route::put('update-password', [AuthorAuthController::class, 'updatePassword'])->name('auth.update-password');

Route::get('author/logout', [AuthorAuthController::class, 'logout'])->name('auth-author.logout');
Route::get('logout', [AuthLoginController::class, 'logout'])->name('auth.logout');



});
Route::view('lsl', 'cms.search.index');


// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
